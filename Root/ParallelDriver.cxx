#include <DiTauRecValidation/ParallelDriver.h>

#include <cstdlib>
#include <fstream>
#include <sstream>
#include <unistd.h>
#include <TSystem.h>
#include <EventLoop/Job.h>
#include <RootCoreUtils/Assert.h>
#include <RootCoreUtils/ThrowMsg.h>

#include "boost/filesystem.hpp"
#include <iostream>

ClassImp(EL::ParallelDriver)

namespace EL
{
  void ParallelDriver ::
  testInvariant () const
  {
    RCU_INVARIANT (this != 0);
  }


  ParallelDriver ::
  ParallelDriver ()
  {
    RCU_NEW_INVARIANT (this);
  }


  void ParallelDriver ::
  batchSubmit (const std::string& location, const SH::MetaObject& options,
               std::size_t njob) const
  {
    RCU_READ_INVARIANT (this);

    {
      std::ostringstream cmd;

      std::string logfile = location + "/worker-log_{1}.txt";
     
      cmd << "parallel " << options.castString(Job::optSubmitFlags)
          << " -j " << std::to_string(nCores)
          << " --load " << std::to_string(max_load) << "%"
          << " --nice " << std::to_string(nice)
          << " --sshloginfile " << clusterConfigFile
          << " --joblog " << location << "/parallel-joblog.txt"
          << " --eta \""
          << "exec 1>> " << logfile << " 2>&1 ; ";
      if(setupScript != "") cmd << "source " << setupScript << "  && ";
      cmd << "cd " << location << "/submit && ./run {1}\""
          << " :::";
     
      std::ostringstream cmdfinal;

      std::vector<size_t> jobs;
      for (unsigned iter = 0, end = njob; iter != end; ++ iter){
        jobs.push_back(iter);
      }
     
      for(size_t iTry = 0; iTry < nRetries + 1; ++iTry){
        cmdfinal << cmd.str();
        for(auto const& jobNr: jobs){
          cmdfinal << " " << jobNr;
        }
       
        if (gSystem->Exec (cmdfinal.str().c_str()) != 0){
          //there are failed jobs, find out which (get fail-X in /fetch and remove fail-X and done-X)
          for(auto iJob = jobs.begin(); iJob != jobs.end(); ){
            if(boost::filesystem::exists(location + "/fetch/fail-"+std::to_string(*iJob))){
              //jobNr failed
              boost::filesystem::remove(location + "/fetch/fail-"+std::to_string(*iJob));
              if(boost::filesystem::exists(location + "/fetch/done-"+std::to_string(*iJob))){
                boost::filesystem::remove(location + "/fetch/done-"+std::to_string(*iJob));
              }
              ++iJob;
            }else{
              iJob = jobs.erase(iJob);
            }
          }
          std::cout << "\033[31;1m" << jobs.size() << " Jobs failed: ";
          for(auto const& jobNr : jobs) std::cout << jobNr << " ";
          std::cout << "\033[0m" << std::endl;
         
          if(iTry < nRetries) std::cout << "Retry " << iTry + 1 << std::endl;

        }else{
          break;
        }

      }

    }
  }
}
 