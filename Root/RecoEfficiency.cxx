#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <DiTauRecValidation/RecoEfficiency.h>
#include "xAODEventInfo/EventInfo.h"
#include "xAODTau/DiTauJetContainer.h"
// #include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/Vertex.h"



// Infrastructure includes:
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/Message.h"

// this is needed to distribute the algorithm to the workers
ClassImp(RecoEfficiency)

/// Helper macro for checking xAOD::TReturnCode
#define EL_RETURN_CHECK( CONTEXT, EXP )             \
  do {                                              \
    if( !EXP.isSuccess() ) {                        \
      Error(CONTEXT,                                \
            XAOD_MESSAGE("Failed to execute: %s"),  \
            #EXP);                                  \
      return EL::StatusCode::FAILURE;               \
    }  \
  } while( false )

RecoEfficiency :: RecoEfficiency ()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



EL::StatusCode RecoEfficiency :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.

  job.useXAOD();
  EL_RETURN_CHECK("setupJob()", xAOD::Init());  // call before opening first file

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode RecoEfficiency :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.
  h_ditau_pt = new TH1F("h_ditau_pt", "h_ditau_pt", 200, 0, 2000); // ditau pt [GeV]
  h_ditau_true_pt = new TH1F("h_ditau_true_pt",  "h_ditau_true_pt", 200, 0, 2000);
  h_ditau_match_true_pt = new TH1F("h_ditau_match_true_pt", "h_ditau_match_true_pt", 200, 0, 2000);
  h_ditau_true_dR = new TH1F("h_ditau_true_dR", "h_ditau_true_dR", 100, 0, 4);
  h_ditau_match_true_dR = new TH1F("h_ditau_match_true_dR", "h_ditau_match_true_dR", 100, 0, 4);
  h_lead_subjet_pt = new TH1F("h_lead_subjet_pt", "h_lead_subjet_pt", 200, 0, 2000);
  h_subl_subjet_pt = new TH1F("h_subl_subjet_pt", "h_subl_subjet_pt", 200, 0, 2000);
  
  h_ditau_lead_subjet_fcore = new TH1F("h_ditau_lead_subjet_fcore", "h_ditau_lead_subjet_fcore", 200, 0, 1);
  h_ditau_subl_subjet_fcore = new TH1F("h_ditau_subl_subjet_fcore", "h_ditau_subl_subjet_fcore", 200, 0, 1);
  h_ditau_match_lead_subjet_fcore = new TH1F("h_ditau_match_lead_subjet_fcore", "h_ditau_match_lead_subjet_fcore", 200, 0, 1);
  h_ditau_match_subl_subjet_fcore = new TH1F("h_ditau_match_subl_subjet_fcore", "h_ditau_match_subl_subjet_fcore", 200, 0, 1);


  wk()->addOutput(h_ditau_pt);
  wk()->addOutput(h_ditau_true_pt);
  wk()->addOutput(h_ditau_match_true_pt);
  wk()->addOutput(h_ditau_true_dR);
  wk()->addOutput(h_ditau_match_true_dR);
  wk()->addOutput(h_lead_subjet_pt);
  wk()->addOutput(h_subl_subjet_pt);
  wk()->addOutput(h_ditau_lead_subjet_fcore);
  wk()->addOutput(h_ditau_subl_subjet_fcore);
  wk()->addOutput(h_ditau_match_lead_subjet_fcore);
  wk()->addOutput(h_ditau_match_subl_subjet_fcore);
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode RecoEfficiency :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode RecoEfficiency :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode RecoEfficiency :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.
  xAOD::TEvent* event = wk()->xaodEvent();

  Info("initialize()", "Number of events = %lli", event->getEntries());
  m_eventCounter = 0;

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode RecoEfficiency :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  xAOD::TEvent* event = wk()->xaodEvent();
  if (m_eventCounter % 100 == 0) Info("execute()", "Event number = %i", m_eventCounter);
  m_eventCounter++;

  // get ditau container
  const xAOD::DiTauJetContainer* xDiTauContainer = 0;
  EL_RETURN_CHECK("execute()", event->retrieve(xDiTauContainer, "DiTauJets"));

  // get truth taus
  std::vector<TLorentzVector*> vTruthTaus;
  EL::StatusCode sc = getTruthTaus(vTruthTaus);
  if (sc == EL::StatusCode::FAILURE) {
    Error("execute()", "Failed to get truth tau info. Exiting.");
    return EL::StatusCode::FAILURE;
  }

  for (const auto* ditau: *xDiTauContainer){
    h_ditau_lead_subjet_fcore->Fill(ditau->fCore(0));
    h_ditau_subl_subjet_fcore->Fill(ditau->fCore(1));
    // const xAOD::Jet* pJetSeed = (*ditau->jetLink());
    // double pt = pJetSeed->pt();
    // Info("execute()", "seed pt: %f", pt);

    const xAOD::Vertex* pVertex = 0;
    pVertex = (*ditau->vertexLink());
    if (pVertex == NULL)
      Error("execute()", "no vertexLink found");
    
  }

  if (vTruthTaus.size() != 2) {
    Info("execute()", "Number of taus = %i. Skip event", vTruthTaus.size());
    return EL::StatusCode::SUCCESS;
  }

  double true_ditau_pt = vTruthTaus[0]->Pt() + vTruthTaus[1]->Pt();
  double true_ditau_dR = vTruthTaus[0]->DeltaR(*vTruthTaus[1]);

  h_ditau_true_pt->Fill(true_ditau_pt/GeV);
  h_ditau_true_dR->Fill(true_ditau_dR);

  // truth match
  for (const auto* ditau: *xDiTauContainer) {
    h_ditau_pt->Fill(ditau->pt()/GeV);

    if ( !isTruthMatched(ditau, vTruthTaus )) continue;
    
    h_ditau_match_true_pt->Fill(true_ditau_pt/GeV);
    h_ditau_match_true_dR->Fill(true_ditau_dR);

    h_ditau_match_lead_subjet_fcore->Fill(ditau->fCore(0));
    h_ditau_match_subl_subjet_fcore->Fill(ditau->fCore(1));
  }



  return EL::StatusCode::SUCCESS;
}



EL::StatusCode RecoEfficiency :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode RecoEfficiency :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  // xAOD::TEvent* event = wk()->xaodEvent();
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode RecoEfficiency :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode RecoEfficiency :: getTruthTaus (std::vector<TLorentzVector*>& vTruthTaus)
{
  // helper function to get a vector of truth taus per event
  xAOD::TEvent* event = wk()->xaodEvent();

  // get truth container
  // const xAOD::TruthEventContainer* xTruthEventContainer = 0;
  // EL_RETURN_CHECK("getTruthTaus()", event->retrieve(xTruthEventContainer, "TruthEvents"));

  const xAOD::TruthParticleContainer* xTruthParticleContainer = 0;
  EL_RETURN_CHECK("getTruthTaus()", event->retrieve(xTruthParticleContainer, "TruthParticles"));

  for (const auto* tp: *xTruthParticleContainer) {
    if (!(tp->absPdgId() == 15 && tp->status() == 2)) continue;

    TLorentzVector* tlv = new TLorentzVector();
    tlv->SetPtEtaPhiE(tp->pt(), tp->eta(), tp->phi(), tp->e());
    vTruthTaus.push_back(tlv);
    // Info("getTruthTaus()"," pt = %.2f, eta = %.2f, phi = %.2f", tlv->Pt(), tlv->Eta(), tlv->Phi());
  }


  return EL::StatusCode::SUCCESS;
}


bool RecoEfficiency :: isTruthMatched (const xAOD::DiTauJet* xDiTau, std::vector<TLorentzVector*> vTruthTaus) 
{
  if (xDiTau->nSubjets() < 2) return false;

  bool match[2] = {false, false} ;
  TLorentzVector* tlvSubjet = new TLorentzVector();

  // loop over subjets
  for (int i = 0; i < 2; ++i)
  {
    tlvSubjet->SetPtEtaPhiE(xDiTau->subjetPt(i), xDiTau->subjetEta(i), xDiTau->subjetPhi(i), xDiTau->subjetE(i));
    if (i == 0) h_lead_subjet_pt->Fill(tlvSubjet->Pt()/GeV);
    if (i == 1) h_subl_subjet_pt->Fill(tlvSubjet->Pt()/GeV);

    // loop over true taus
    for (int j = 0; j < 2; ++j)
    {
      if (vTruthTaus[j]->DeltaR(*tlvSubjet) < 0.2) {
        match[j] = true;
        break;
      }
    }
  }


  if (match[0] == true && match[1] == true){
    return true;
  }
  else
    return false;
}
