#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <DiTauRecValidation/TriggerAnalysis.h>
// #include <TSystem.h>

// EDM includes
#include "xAODEventInfo/EventInfo.h"
#include "xAODTau/DiTauJetContainer.h"
#include "xAODTruth/TruthParticleContainer.h"

// Tools
#include "TauAnalysisTools/TauTruthMatchingTool.h"
#include "DiTauID/DiTauIDVarCalculator.h"
#include "DiTauID/DiTauDiscriminant.h"

// Infrastructure includes:
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/Message.h"
#include "EventLoop/OutputStream.h"
#include "AsgTools/MsgStream.h"
#include "AsgTools/MsgStreamMacros.h"

// Root
#include "TFile.h"


typedef std::vector< ElementLink< xAOD::TrackParticleContainer > >  TrackParticleLinks_t;

// this is needed to distribute the algorithm to the workers
ClassImp(TriggerAnalysis)

/// helper macros for checking and retriving xAOD containers
#define CHECKTOOL( ARG )                    \
  do {                                                  \
    const bool result = ARG;                \
    if( ! result ) {                    \
      ::Error( "TriggerAnalysis", "Failed to execute: \"%s\"",  \
           #ARG );                  \
      return 1;                     \
    }                           \
  } while( false )

#define CHECK( ARG )                    \
  do {                                                  \
    const EL::StatusCode result = ARG;                \
    if( result == EL::StatusCode::FAILURE ) {                    \
      ::Error( "TriggerAnalysis", "Failed to execute: \"%s\"",  \
           #ARG );                  \
      return 1;                     \
    }                           \
  } while( false )

#define EL_RETURN_CHECK( CONTEXT, EXP )             \
  do {                                              \
    if( !EXP.isSuccess() ) {                        \
      Error(CONTEXT,                                \
            XAOD_MESSAGE("Failed to execute: %s"),  \
            #EXP);                                  \
      return EL::StatusCode::FAILURE;               \
    }  \
  } while( false )



TriggerAnalysis::TriggerAnalysis ()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
  m_trigDecisionTool = 0;
}



EL::StatusCode TriggerAnalysis::setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.
  Info("setupJob()", "setupJob");
  job.useXAOD();
  EL_RETURN_CHECK("setupJob()", xAOD::Init());  // call before opening first file

  xAOD::TEvent::EAuxMode mode = xAOD::TEvent::kBranchAccess; // alternative: xAOD::TEvent::kClassAccess;
  xAOD::TEvent event(mode);

  EL::OutputStream out("trigger", "xAOD");
  job.outputAdd(out);

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerAnalysis::histInitialize ()
{
  Info("histInitialize()", "histInitialize");
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerAnalysis::fileExecute ()
{
  Info("fileExecute()", "fileExecute");
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerAnalysis::changeInput (bool firstFile)
{
  Info("changeInput()", "changeInput");
  xAOD::TEvent* event = wk()->xaodEvent();
  
  const xAOD::EventInfo* eventInfo = 0;
  EL_RETURN_CHECK("changeInput", event->retrieve( eventInfo, "EventInfo"));

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerAnalysis::initialize ()
{
  xAOD::TEvent* event = wk()->xaodEvent();
    
  // event selector
  m_evtSelector = new EventSelector("evtselector_for_triggeranalysis");
  m_evtSelector->initialize();

  // write to output xAOD
  TFile *file = wk()->getOutputFile("trigger");
  EL_RETURN_CHECK("initialize", event->writeTo(file));

  // trigger tools
  m_trigConfigTool = new TrigConf::xAODConfigTool("xAODConfigTool");
  m_trigConfigTool->msg().setLevel(MSG::WARNING);
  EL_RETURN_CHECK("initialize()", m_trigConfigTool->initialize() );
  ToolHandle< TrigConf::ITrigConfigTool > trigConfigHandle( m_trigConfigTool );
  m_trigDecisionTool = new Trig::TrigDecisionTool("TrigDecisionTool");
  m_trigDecisionTool->msg().setLevel(MSG::WARNING);
  EL_RETURN_CHECK("initialize()", m_trigDecisionTool->setProperty( "ConfigTool", trigConfigHandle ) );
  EL_RETURN_CHECK("initialize()", m_trigDecisionTool->setProperty( "TrigDecisionKey", "xTrigDecision" ) );
  EL_RETURN_CHECK("initialize()", m_trigDecisionTool->initialize() );

  // Trigger Matching Tool
  m_tmt = new Trig::MatchingTool("Trig::MatchingTool/DiTauJetMatchingToolforAnalysis");
  m_tmt->msg().setLevel(MSG::WARNING);
  EL_RETURN_CHECK("initialize", m_tmt->initialize());


  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerAnalysis::execute ()
{
  // get event
  xAOD::TEvent* event = wk()->xaodEvent();

  // get event info container
  const xAOD::EventInfo* eventInfo = 0;
  EL_RETURN_CHECK("execute",event->retrieve( eventInfo, "EventInfo"));

  // // check good run list
  if (! m_evtSelector->checkGRL(event))
  {
    return EL::StatusCode::SUCCESS;
  }

  // ----------------------------------------------------------------------------
  // do awesome things
  // ----------------------------------------------------------------------------
  const xAOD::DiTauJetContainer* xDiTauContainer = 0;
  EL_RETURN_CHECK( "execute()", 
      event->retrieve(xDiTauContainer, "DiTauJets") );

  const xAOD::JetContainer* xJetContainer = 0;
  EL_RETURN_CHECK( "execute()", 
      event->retrieve(xJetContainer, "AntiKt10LCTopoJets") );

  // const xAOD::JetContainer* hltJetContainer = 0;
  // EL_RETURN_CHECK( "execute()",
  //     event->retrieve(hltJetContainer, "HLT_xAOD__JetContainer_a10tcemnojcalibFS") );

  // for (const auto xJet: *hltJetContainer)
  // {
  //   Info("execute()", "hlt_jet pt: %.1f", xJet->pt());
  // }


  // const xAOD::JetContainer* xJetContainer = 0;
  // EL_RETURN_CHECK( "execute()", 
  //     event->retrieve(xJetContainer, "AntiKt4LCTopoJets"));

  std::vector<const xAOD::IParticle*> jets;

  std::string triggers[] = {"HLT_j360_a10_nojcalib", "HLT_j460_a10_nojcalib", "HLT_j100", "HLT_j110", "HLT_j150", "HLT_j175", "HLT_j200", "HLT_j260", "HLT_j300", "HLT_j320", "HLT_j380", "HLT_j400"};

  // trigger matching loop over fat jets
  // for (const auto xJet: *xJetContainer)
  // {
  //   jets.clear();
  //   jets.push_back(xJet);

  //   for (auto trig: triggers)
  //   {
  //     auto cg = m_trigDecisionTool->getChainGroup(trig);
  //     bool fired = cg->isPassed();
  //     int match = m_tmt->match(jets, trig.c_str(), 0.5);

  //     if ((!fired) || match < 1)
  //     {
  //       xJet->auxdecor< int >( trig.c_str() ) = 0;
  //       xJet->auxdecor< double >( (trig + "_prescale").c_str() ) = 1;
  //       continue;
  //     }

  //     xJet->auxdecor< int >( trig.c_str() ) = match;
  //     xJet->auxdecor< double >( (trig + "_prescale").c_str() ) = cg->getPrescale();

  //   }
  // }

  // const std::string tag_trigger = "HLT_j150";

  // auto cg = m_trigDecisionTool->getChainGroup(tag_trigger);
  // bool trig_fired = cg->isPassed();
  // double prescale = cg->getPrescale();


  // if (!trig_fired)
  // {
  //   // Info("execute()", "trigger %s (prescale: %.1f) did not fire. Skip event", trigger.c_str(), prescale);
  //   return EL::StatusCode::SUCCESS;
  // }
  // // Info("execute()", "trigger %s (prescale: %.1f) did fire.", trigger.c_str(), prescale);

  // int i = 0;
  // for (const auto xJet: *xJetContainer)
  // {
  //   jets.clear();
  //   jets.push_back(xJet);
  //   xJet->auxdecor< int >("nr") = i;
  //   xJet->auxdecor< std::string >( "tag_trigger" ) = tag_trigger;

  //   for (auto trig: triggers)
  //   {
  //     int match = m_tmt->match(jets, trig.c_str(), 0.5);
  //     xJet->auxdecor< int >( trig.c_str() ) = match;
  //     xJet->auxdecor< double >( "prescale" ) = prescale;
  //   }

  //   i++;
  // }
  // ----------------------------------------------------------------------------

  for (auto trig: triggers)
  {
    auto cg = m_trigDecisionTool->getChainGroup(trig);
    bool trig_fired = cg->isPassed();
    double prescale = cg->getPrescale();

    for (const auto xJet: *xJetContainer)
    {
      xJet->auxdecor< int >(trig.c_str()) = trig_fired;
      xJet->auxdecor< double >((trig+"_prescale").c_str()) = prescale;
    }
  }

  int i = 0;
  for (const auto xJet: *xJetContainer)
  {
    xJet->auxdecor<int>("nr") = i;
    i++;
  }

  // ----------------------------------------------------------------------------
  

  // fill event in xAOD output
  EL_RETURN_CHECK("execute()", event->copy("DiTauJets"));
  EL_RETURN_CHECK("execute()", event->copy("AntiKt10LCTopoJets"));
  EL_RETURN_CHECK("execute()", event->copy("AntiKt4LCTopoJets"));
  EL_RETURN_CHECK("execute()", event->copy("EventInfo"));
  event->fill();

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerAnalysis::postExecute ()
{
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerAnalysis::finalize ()
{
  Info("finalize()", "finalize()");
  xAOD::TEvent* event = wk()->xaodEvent();

  TFile *file = wk()->getOutputFile("trigger");
  EL_RETURN_CHECK("finalize", event->finishWritingTo(file));

  if (m_trigDecisionTool)
  {
    Info("finalize()", "delete m_trigDecisionTool.");
    delete m_trigDecisionTool;
    m_trigDecisionTool = 0;
  }
  if (m_trigConfigTool)
  {
    delete m_trigConfigTool;
    m_trigConfigTool = 0;
  }
  if (m_tmt)
  {
    delete m_tmt;
    m_tmt = 0;
  }
  if (m_evtSelector)
  {
    delete m_evtSelector;
    m_evtSelector = 0;
  }

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode TriggerAnalysis::histFinalize ()
{
  return EL::StatusCode::SUCCESS;
}

