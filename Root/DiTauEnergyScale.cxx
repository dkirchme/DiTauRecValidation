#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <DiTauRecValidation/DiTauEnergyScale.h>

// EDM includes
#include "xAODEventInfo/EventInfo.h"
#include "xAODTau/DiTauJetContainer.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODTruth/TruthParticleContainer.h"

// Infrastructure includes:
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/Message.h"
#include <AsgTools/MessageCheck.h>
#include "EventLoop/OutputStream.h"
#include "xAODCore/ShallowCopy.h"

// this is needed to distribute the algorithm to the workers
ClassImp(DiTauEnergyScale)

/// helper macros for checking and retriving xAOD containers
#define CHECKTOOL( ARG )                    \
  do {                                                  \
    const bool result = ARG;                \
    if( ! result ) {                    \
      ::Error( "calcDiTauVariables", "Failed to execute: \"%s\"",  \
           #ARG );                  \
      return 1;                     \
    }                           \
  } while( false )

#define CHECK( ARG )                    \
  do {                                                  \
    const EL::StatusCode result = ARG;                \
    if( result == EL::StatusCode::FAILURE ) {                    \
      ::Error( "IDVariables", "Failed to execute: \"%s\"",  \
           #ARG );                  \
      return 1;                     \
    }                           \
  } while( false )

#define EL_RETURN_CHECK( CONTEXT, EXP )             \
  do {                                              \
    if( !EXP.isSuccess() ) {                        \
      Error(CONTEXT,                                \
            XAOD_MESSAGE("Failed to execute: %s"),  \
            #EXP);                                  \
      return EL::StatusCode::FAILURE;               \
    }  \
  } while( false )


typedef std::vector< ElementLink< xAOD::TrackParticleContainer > >  TrackParticleLinks_t;


// ----------------------------------------------------------------------------
DiTauEnergyScale::DiTauEnergyScale ()
{
}


// ----------------------------------------------------------------------------
EL::StatusCode DiTauEnergyScale::setupJob (EL::Job& job)
{
  ANA_CHECK_SET_TYPE( EL::StatusCode );
  job.useXAOD();
  ANA_CHECK( xAOD::Init()) ;  // call before opening first file

  xAOD::TEvent::EAuxMode mode = xAOD::TEvent::kClassAccess; // alternative: xAOD::TEvent::kBranchAccess;
  xAOD::TEvent event(mode);


  return EL::StatusCode::SUCCESS;
}


// ----------------------------------------------------------------------------
EL::StatusCode DiTauEnergyScale::histInitialize ()
{
  // ----------------------------------------------------------------------------
  // histograms for reco efficiency 
  // ----------------------------------------------------------------------------
  h_ditau_true_pt = new TH1F("h_ditau_true_pt",
          ";true ditau p_{T} [GeV]", 200, 0, 2000);
  h_ditau_match_true_pt = new TH1F("h_ditau_match_true_pt",
          ";true ditau p_{T} [GeV]", 200, 0, 2000);
  h_ditau_true_pt_vis = new TH1F("h_ditau_true_pt_vis",
          ";true vis ditau p_{T} [GeV]", 200, 0, 2000);
  h_ditau_match_true_pt_vis = new TH1F("h_ditau_match_true_pt_vis",
          ";true vis ditau p_{T} [GeV]", 200, 0, 2000);
  h_ditau_true_dR = new TH1F("h_ditau_true_dR",
          ";#DeltaR", 100, 0, 4);
  h_ditau_match_true_dR = new TH1F("h_ditau_match_true_dR",
          ";#DeltaR", 100, 0, 4);
  h_ditau_true_mu = new TH1F("h_ditau_true_mu", 
          ";#mu", 50, 0, 50);
  h_ditau_match_true_mu = new TH1F("h_ditau_match_true_mu", 
          ";#mu", 50, 0, 50);

  // ----------------------------------------------------------------------------
  // histograms for energy differences in DiTauRec
  // ----------------------------------------------------------------------------
  // 1 dim absolute delta pt
  h_delPt_ditau = new TH1F("h_delPt_ditau", 
          ";reco ditau p_{T} - true vis ditau p_{T} [GeV];", 
          200, -200, 200);
  h_delPt_subjet = new TH1F("h_delPt_subjet", 
          ";subjet p_{T} - true vis tau p_{T} [GeV];", 
          200, -200, 200);
  h_delPt_lead_subjet = new TH1F("h_delPt_lead_subjet", 
          ";leading subjet p_{T} - true vis tau p_{T} [GeV];", 
          200, -200, 200);
  h_delPt_subl_subjet = new TH1F("h_delPt_subl_subjet", 
          ";subleading subjet p_{T} - true vis tau p_{T} [GeV];", 
          200, -200, 200);
  
  // 1 dim relative delta pt
  h_delPtRel_ditau = new TH1F("h_delPtRel_ditau", 
          ";(reco ditau p_{T} - true vis ditau p_{T}) / true vis ditau p_{T};",
          200, -1, 1);
  h_delPtRel_subjet = new TH1F("h_delPtRel_subjet", 
          ";(subjet p_{T} - true vis tau p_{T}) / true vis tau p_{T};",
          200, -1, 1);
  h_delPtRel_lead_subjet = new TH1F("h_delPtRel_lead_subjet", 
          ";(leading subjet p_{T} - true vis tau p_{T}) / true vis tau p_{T};",
          200, -1, 1);
  h_delPtRel_subl_subjet = new TH1F("h_delPtRel_subl_subjet", 
          ";(subleading subjet p_{T} - true vis tau p_{T}) / true vis tau p_{T};",
          200, -1, 1);
  
  // 2 dim absolute delta pt vs true pt
  h2_delPt_pt_ditau = new TH2F("h2_delPt_pt_ditau", 
          ";true vis ditau p_{T} [GeV];reco ditau p_{T} - true vis ditau p_{T} [GeV];", 
          20, 0, 1000, 20, -200, 200);
  h2_delPt_pt_subjet = new TH2F("h2_delPt_pt_subjet", 
          ";true vis tau p_{T} [GeV];subjet p_{T} - true vis tau p_{T} [GeV];", 
          20, 0, 1000, 20, -200, 200);
  h2_delPt_pt_lead_subjet = new TH2F("h2_delPt_pt_lead_subjet", 
          ";true vis tau p_{T} [GeV];leading subjet p_{T} - true vis tau p_{T} [GeV];", 
          20, 0, 1000, 20, -200, 200);
  h2_delPt_pt_subl_subjet = new TH2F("h2_delPt_pt_subl_subjet", 
          ";true vis tau p_{T} [GeV];subleading subjet p_{T} - true vis tau p_{T} [GeV];",
          20, 0, 1000, 20, -200, 200);

  // 2 dim absolute delta pt vs mu
  h2_delPt_mu_ditau = new TH2F("h2_delPt_mu_ditau", 
          ";#mu;reco ditau p_{T} - true vis ditau p_{T} [GeV]", 
          25, 0, 50, 20, -200, 200);
  h2_delPt_mu_subjet = new TH2F("h2_delPt_mu_subjet", 
          ";#mu;subjet p_{T} - true vis tau p_{T} [GeV];",
          25, 0, 50, 20, -200, 200);
  h2_delPt_mu_lead_subjet = new TH2F("h2_delPt_mu_lead_subjet", 
          ";#mu;leading subjet p_{T} - true vis tau p_{T} [GeV];",
          25, 0, 50, 20, -200, 200);
  h2_delPt_mu_subl_subjet = new TH2F("h2_delPt_mu_subl_subjet", 
          ";#mu;subleading subjet p_{T} - true vis tau p_{T} [GeV];",
          25, 0, 50, 20, -200, 200);

  // 2 dim absolute delta pt vs eta
  h2_delPt_eta_subjet = new TH2F("h2_delPt_eta_subjet",
          ";#eta;subjet p_{T} - true vis ditau p_{T} [GeV]", 
          30, -3, 3, 20, -200, 200);

  // 2 dim fraction reco pt / true pt vs reco pt 
  h2_fracPt_recoPt_subjet = new TH2F("h2_fracPt_recoPt_subjet",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  h2_fracPt_recoPt_subjet_1p = new TH2F("h2_fracPt_recoPt_subjet_1p",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  h2_fracPt_recoPt_subjet_mp = new TH2F("h2_fracPt_recoPt_subjet_mp",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  // 2 dim fraction reco pt / true pt vs reco pt for different eta
  h2_fracPt_recoPt_subjet_1p_00_03 = new TH2F("h2_fracPt_recoPt_subjet_1p_00_03",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  h2_fracPt_recoPt_subjet_1p_03_08 = new TH2F("h2_fracPt_recoPt_subjet_1p_03_08",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  h2_fracPt_recoPt_subjet_1p_08_13 = new TH2F("h2_fracPt_recoPt_subjet_1p_08_13",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  h2_fracPt_recoPt_subjet_1p_13_16 = new TH2F("h2_fracPt_recoPt_subjet_1p_13_16",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  h2_fracPt_recoPt_subjet_1p_16_24 = new TH2F("h2_fracPt_recoPt_subjet_1p_16_24",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  h2_fracPt_recoPt_subjet_mp_00_03 = new TH2F("h2_fracPt_recoPt_subjet_mp_00_03",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  h2_fracPt_recoPt_subjet_mp_03_08 = new TH2F("h2_fracPt_recoPt_subjet_mp_03_08",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  h2_fracPt_recoPt_subjet_mp_08_13 = new TH2F("h2_fracPt_recoPt_subjet_mp_08_13",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  h2_fracPt_recoPt_subjet_mp_13_16 = new TH2F("h2_fracPt_recoPt_subjet_mp_13_16",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  h2_fracPt_recoPt_subjet_mp_16_24 = new TH2F("h2_fracPt_recoPt_subjet_mp_16_24",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);

  // 2 dim relative delta pt vs true pt for different eta
  h2_relPt_pt_subjet_1p_00_03 = new TH2F("h2_relPt_pt_subjet_1p_00_03",
      ";p_{T,vis}^{true} [GeV];(p_{T,LC}^{#tau} - p_{T,vis}^{true}) / p_{T,vis}^{true};", 
          20, 0, 1000, 20, -1, 1);
  h2_relPt_pt_subjet_1p_03_08 = new TH2F("h2_relPt_pt_subjet_1p_03_08",
      ";p_{T,vis}^{true} [GeV];(p_{T,LC}^{#tau} - p_{T,vis}^{true}) / p_{T,vis}^{true};", 
          20, 0, 1000, 20, -1, 1);
  h2_relPt_pt_subjet_1p_08_13 = new TH2F("h2_relPt_pt_subjet_1p_08_13",
      ";p_{T,vis}^{true} [GeV];(p_{T,LC}^{#tau} - p_{T,vis}^{true}) / p_{T,vis}^{true};", 
          20, 0, 1000, 20, -1, 1);
  h2_relPt_pt_subjet_1p_13_16 = new TH2F("h2_relPt_pt_subjet_1p_13_16",
      ";p_{T,vis}^{true} [GeV];(p_{T,LC}^{#tau} - p_{T,vis}^{true}) / p_{T,vis}^{true};", 
          20, 0, 1000, 20, -1, 1);
  h2_relPt_pt_subjet_1p_16_24 = new TH2F("h2_relPt_pt_subjet_1p_16_24",
      ";p_{T,vis}^{true} [GeV];(p_{T,LC}^{#tau} - p_{T,vis}^{true}) / p_{T,vis}^{true};", 
          20, 0, 1000, 20, -1, 1);
  h2_relPt_pt_subjet_mp_00_03 = new TH2F("h2_relPt_pt_subjet_mp_00_03",
      ";p_{T,vis}^{true} [GeV];(p_{T,LC}^{#tau} - p_{T,vis}^{true}) / p_{T,vis}^{true};", 
          20, 0, 1000, 20, -1, 1);
  h2_relPt_pt_subjet_mp_03_08 = new TH2F("h2_relPt_pt_subjet_mp_03_08",
      ";p_{T,vis}^{true} [GeV];(p_{T,LC}^{#tau} - p_{T,vis}^{true}) / p_{T,vis}^{true};", 
          20, 0, 1000, 20, -1, 1);
  h2_relPt_pt_subjet_mp_08_13 = new TH2F("h2_relPt_pt_subjet_mp_08_13",
      ";p_{T,vis}^{true} [GeV];(p_{T,LC}^{#tau} - p_{T,vis}^{true}) / p_{T,vis}^{true};", 
          20, 0, 1000, 20, -1, 1);
  h2_relPt_pt_subjet_mp_13_16 = new TH2F("h2_relPt_pt_subjet_mp_13_16",
      ";p_{T,vis}^{true} [GeV];(p_{T,LC}^{#tau} - p_{T,vis}^{true}) / p_{T,vis}^{true};", 
          20, 0, 1000, 20, -1, 1);
  h2_relPt_pt_subjet_mp_16_24 = new TH2F("h2_relPt_pt_subjet_mp_16_24",
      ";p_{T,vis}^{true} [GeV];(p_{T,LC}^{#tau} - p_{T,vis}^{true}) / p_{T,vis}^{true};", 
          20, 0, 1000, 20, -1, 1);


  // ----------------------------------------------------------------------------
  // histograms for energy differences in tauRec
  // ----------------------------------------------------------------------------
  // 1 dim absolute delta pt 
  h_delPt_tau = new TH1F("h_delPt_tau", 
          ";standard reco tau p_{T} - true vis tau p_{T} [GeV]",
          200, -200, 200);

  // 1 dim relative delta pt 
  h_delPtRel_tau = new TH1F("h_delPtRel_tau", 
          ";(standard reco tau p_{T} - true vis tau p_{T}) / true vis tau p_{T}",
          200, -1, 1);

  // 2 dim absolute delta pt vs true pt
  h2_delPt_pt_tau = new TH2F("h2_delPt_pt_tau", 
          ";true vis tau p_{T} [GeV];standard reco tau p_{T} - true vis tau p_{T} [GeV]",
          20, 0, 1000, 20, -200, 200);

  // 2 dim absolute delta pt vs mu
  h2_delPt_mu_tau = new TH2F("h2_delPt_mu_tau", 
          ";#mu;standard reco tau p_{T} - true vis tau p_{T} [GeV];",
          25, 0, 50, 20, -200, 200);

  // 2 dim absolute delta pt vs eta
  h2_delPtRel_eta_tau = new TH2F("h2_delPtRel_eta_tau",
          ";#eta;standard reco tau p_{T} - true vis tau p_{T} [GeV];",
          30, -3, 3, 20, -200, 200);

  // ----------------------------------------------------------------------------
  // add histupgrams to worker
  // ----------------------------------------------------------------------------
  // histograms for reco efficiency
  wk()->addOutput(h_ditau_true_pt);
  wk()->addOutput(h_ditau_match_true_pt);
  wk()->addOutput(h_ditau_true_pt_vis);
  wk()->addOutput(h_ditau_match_true_pt_vis);
  wk()->addOutput(h_ditau_true_dR);
  wk()->addOutput(h_ditau_match_true_dR);
  wk()->addOutput(h_ditau_true_mu);
  wk()->addOutput(h_ditau_match_true_mu);

  // histograms for energy differences in DiTauRec
  wk()->addOutput(h_delPt_ditau);
  wk()->addOutput(h_delPt_subjet);
  wk()->addOutput(h_delPt_lead_subjet);
  wk()->addOutput(h_delPt_subl_subjet);

  wk()->addOutput(h_delPtRel_ditau);
  wk()->addOutput(h_delPtRel_subjet);
  wk()->addOutput(h_delPtRel_lead_subjet);
  wk()->addOutput(h_delPtRel_subl_subjet);

  wk()->addOutput(h2_delPt_pt_ditau);
  wk()->addOutput(h2_delPt_pt_subjet);
  wk()->addOutput(h2_delPt_pt_lead_subjet);
  wk()->addOutput(h2_delPt_pt_subl_subjet);

  wk()->addOutput(h2_delPt_mu_ditau);
  wk()->addOutput(h2_delPt_mu_subjet);
  wk()->addOutput(h2_delPt_mu_lead_subjet);
  wk()->addOutput(h2_delPt_mu_subl_subjet);
  wk()->addOutput(h2_delPt_eta_subjet);
  
  wk()->addOutput(h2_fracPt_recoPt_subjet);
  wk()->addOutput(h2_fracPt_recoPt_subjet_1p);
  wk()->addOutput(h2_fracPt_recoPt_subjet_mp);
  wk()->addOutput(h2_fracPt_recoPt_subjet_1p_00_03);
  wk()->addOutput(h2_fracPt_recoPt_subjet_1p_03_08);
  wk()->addOutput(h2_fracPt_recoPt_subjet_1p_08_13);
  wk()->addOutput(h2_fracPt_recoPt_subjet_1p_13_16);
  wk()->addOutput(h2_fracPt_recoPt_subjet_1p_16_24);
  wk()->addOutput(h2_fracPt_recoPt_subjet_mp_00_03);
  wk()->addOutput(h2_fracPt_recoPt_subjet_mp_03_08);
  wk()->addOutput(h2_fracPt_recoPt_subjet_mp_08_13);
  wk()->addOutput(h2_fracPt_recoPt_subjet_mp_13_16);
  wk()->addOutput(h2_fracPt_recoPt_subjet_mp_16_24);

  wk()->addOutput(h2_relPt_pt_subjet_1p_00_03);
  wk()->addOutput(h2_relPt_pt_subjet_1p_03_08);
  wk()->addOutput(h2_relPt_pt_subjet_1p_08_13);
  wk()->addOutput(h2_relPt_pt_subjet_1p_13_16);
  wk()->addOutput(h2_relPt_pt_subjet_1p_16_24);
  wk()->addOutput(h2_relPt_pt_subjet_mp_00_03);
  wk()->addOutput(h2_relPt_pt_subjet_mp_03_08);
  wk()->addOutput(h2_relPt_pt_subjet_mp_08_13);
  wk()->addOutput(h2_relPt_pt_subjet_mp_13_16);
  wk()->addOutput(h2_relPt_pt_subjet_mp_16_24);
  
  // histograms for energy differences in tauRec
  wk()->addOutput(h_delPt_tau);
  wk()->addOutput(h_delPtRel_tau);
  wk()->addOutput(h2_delPt_pt_tau);
  wk()->addOutput(h2_delPt_mu_tau);
  wk()->addOutput(h2_delPtRel_eta_tau);

  return EL::StatusCode::SUCCESS;
}


// ----------------------------------------------------------------------------
EL::StatusCode DiTauEnergyScale::fileExecute ()
{
  return EL::StatusCode::SUCCESS;
}


// ----------------------------------------------------------------------------
EL::StatusCode DiTauEnergyScale::changeInput (bool firstFile)
{
  return EL::StatusCode::SUCCESS;
}


// ----------------------------------------------------------------------------
EL::StatusCode DiTauEnergyScale::initialize ()
{
  ANA_CHECK_SET_TYPE( EL::StatusCode );

  xAOD::TEvent* event = wk()->xaodEvent();

  // Info("initialize()", "Number of events = %lli", event->getEntries());
  // m_eventCounter = 0;

  // write to output xAOD
  // TFile *file = wk()->getOutputFile("outputLabel");
  // ANA_CHECK( event->writeTo(file) );

  m_evtSelector = new EventSelector("evtselector_for_dtes");
  ANA_CHECK(m_evtSelector->initialize());

  return EL::StatusCode::SUCCESS;
}


// ----------------------------------------------------------------------------
EL::StatusCode DiTauEnergyScale::execute ()
{
  ANA_CHECK_SET_TYPE( EL::StatusCode );
  // print event counter
  xAOD::TEvent* event = wk()->xaodEvent();

  if (! m_evtSelector->checkEvent(event))
  {
    return EL::StatusCode::SUCCESS;
  }

    xAOD::TStore* store = wk()->xaodStore();

  Info("execute()", "IDVariables store:");

  store->print();



  // print event counter
  // if (m_eventCounter % 100 == 0) Info("execute()", "Event number = %i", m_eventCounter);
  // m_eventCounter++;

  // get ditau container
  const xAOD::DiTauJetContainer* xDiTauContainer = 0;
  ANA_CHECK( event->retrieve(xDiTauContainer, "Bla") );

  // prepare shallow copy
  // std::pair< xAOD::DiTauJetContainer*, xAOD::ShallowAuxContainer* >xDiTauShallowCont = xAOD::shallowCopyContainer(*xDiTauContainer);

  // ANA_CHECK( event->retrieve(xDiTauShallowCont.first, "_DiTauJets") );
  // ANA_CHECK( event->retrieve(xDiTauShallowCont.second, "_DiTauJetsAux.") );


  // get tau container
  const xAOD::TauJetContainer* xTauContainer = 0;
  ANA_CHECK( event->retrieve(xTauContainer, "TauJets") );

  CHECK(plotEnergyDiff(event, xDiTauContainer, xTauContainer));

  // event->fill();

  return EL::StatusCode::SUCCESS;
}


// ----------------------------------------------------------------------------
EL::StatusCode DiTauEnergyScale::postExecute ()
{
  return EL::StatusCode::SUCCESS;
}


// ----------------------------------------------------------------------------
EL::StatusCode DiTauEnergyScale::finalize ()
{
  // xAOD::TEvent* event = wk()->xaodEvent();

  // TFile *file = wk()->getOutputFile("outputLabel");
  // ANA_CHECK(  event->finishWritingTo(file) );

  if (m_evtSelector)
  {
    delete m_evtSelector;
    m_evtSelector = 0;
  }


  return EL::StatusCode::SUCCESS;
}


// ----------------------------------------------------------------------------
EL::StatusCode DiTauEnergyScale::histFinalize ()
{
  return EL::StatusCode::SUCCESS;
}


// ----------------------------------------------------------------------------
EL::StatusCode DiTauEnergyScale::plotEnergyDiff (xAOD::TEvent* event,
                const xAOD::DiTauJetContainer* xDiTauContainer,
                const xAOD::TauJetContainer* xTauContainer)
{

  // get EventInfo and pile up
  const xAOD::EventInfo* eventInfo = 0;
  ANA_CHECK(  event->retrieve( eventInfo, "EventInfo") );
  float mu = eventInfo->actualInteractionsPerCrossing();

  // get TruthTau container
  const xAOD::TruthParticleContainer* xTruthTauContainer = 0;
  ANA_CHECK( event->retrieve(xTruthTauContainer, "TruthTaus") );

  // get 2 leading truth taus
  const xAOD::TruthParticle* xTruthTauLead = 0;
  const xAOD::TruthParticle* xTruthTauSubl = 0;
  // ----------------------------------------------------------------------------
  // TODO: put in extra method; pass xTruthTauLead and xTruthTauSubl as reference
  for (const auto* xTruthTau: *xTruthTauContainer)
  {
    if ( !xTruthTauLead || xTruthTauLead->pt() < xTruthTau->pt() ) 
    {
      xTruthTauSubl = xTruthTauLead;
      xTruthTauLead = xTruthTau;
      continue;
    }
    if ( !xTruthTauSubl || xTruthTauSubl->pt() < xTruthTau->pt() )
    {
      xTruthTauSubl = xTruthTau;
    }
  }

  if ( !xTruthTauLead || !xTruthTauSubl )
  {
    return EL::StatusCode::SUCCESS;
  }
  // ----------------------------------------------------------------------------

  double true_ditau_pt = ( xTruthTauLead->p4()+xTruthTauSubl->p4() ).Pt();
  double true_ditau_dR = ( xTruthTauLead->p4().DeltaR(xTruthTauSubl->p4()) );

  double true_ditau_pt_vis = xTruthTauLead->auxdata<double>("pt_vis")
                             + xTruthTauSubl->auxdata<double>("pt_vis");

  h_ditau_true_pt->Fill(true_ditau_pt/GeV);
  h_ditau_true_dR->Fill(true_ditau_dR);
  h_ditau_true_pt_vis->Fill(true_ditau_pt_vis/GeV);
  h_ditau_true_mu->Fill(mu);

  // ----------------------------------------------------------------------------
  // DiTauRec
  // ----------------------------------------------------------------------------
  for (const auto* xDiTau: *xDiTauContainer)
  {
    if (!xDiTau->isAvailable<char>("IsTruthMatched"))
    {
      Warning("plotEnergyDiff()", "truth match info not available");
      continue;
    }

    if (xDiTau->auxdata<char>("IsTruthMatched") == (char)true) 
    {
      h_ditau_match_true_pt->Fill(true_ditau_pt/GeV);
      h_ditau_match_true_dR->Fill(true_ditau_dR);
      h_ditau_match_true_pt_vis->Fill(true_ditau_pt_vis/GeV);
      h_ditau_match_true_mu->Fill(mu);
        
      // diff between true ditau pt and reco pt
      double delta = xDiTau->auxdata<double>("ditau_pt") - true_ditau_pt_vis;
      h_delPt_ditau->Fill(delta/GeV);
      h_delPtRel_ditau->Fill(delta/true_ditau_pt_vis);
      h2_delPt_pt_ditau->Fill(true_ditau_pt_vis/GeV, delta/GeV);

      h2_delPt_mu_ditau->Fill(mu, delta/GeV);

      // ----------------------------------------------------------------------------
      // ----------------------------------------------------------------------------
      // switch, according to reconstructed ditau subjets
      // TODO: not needed if implemented in TruthMatchingTool

      TLorentzVector tlvSubjetLead = TLorentzVector();
      TLorentzVector tlvSubjetSubl = TLorentzVector();

      tlvSubjetLead.SetPtEtaPhiE(xDiTau->subjetPt(0),
                                 xDiTau->subjetEta(0),
                                 xDiTau->subjetPhi(0),
                                 xDiTau->subjetE(0));
      tlvSubjetSubl.SetPtEtaPhiE(xDiTau->subjetPt(1),
                                 xDiTau->subjetEta(1),
                                 xDiTau->subjetPhi(1),
                                 xDiTau->subjetE(1));

      int nTrackLead = getNtracks(xDiTau, 0);
      int nTrackSubl = getNtracks(xDiTau, 1);

      if ( (xTruthTauLead->p4().DeltaR(tlvSubjetSubl) < 0.2) 
          && (xTruthTauSubl->p4().DeltaR(tlvSubjetLead) < 0.2) )
      {
        const xAOD::TruthParticle* xTmp;
        xTmp = xTruthTauLead;
        xTruthTauLead = xTruthTauSubl;
        xTruthTauSubl = xTmp;

        int iTmp = nTrackLead;
        nTrackLead = nTrackSubl;
        nTrackSubl = iTmp;
      }

      // ----------------------------------------------------------------------------
      // ----------------------------------------------------------------------------
      double ptVisLead = xTruthTauLead->auxdata<double>("pt_vis");
      double ptVisSubl = xTruthTauSubl->auxdata<double>("pt_vis");
      double deltaLead = xDiTau->subjetPt(0) - ptVisLead;
      double deltaSubl = xDiTau->subjetPt(1) - ptVisSubl;

      h_delPt_subjet->Fill(deltaLead/GeV);
      h_delPt_subjet->Fill(deltaSubl/GeV);
      h_delPtRel_subjet->Fill(deltaLead/ptVisLead);
      h_delPtRel_subjet->Fill(deltaSubl/ptVisSubl);

      h_delPt_lead_subjet->Fill(deltaLead/GeV);
      h_delPtRel_lead_subjet->Fill(deltaLead/ptVisLead);
      h_delPt_subl_subjet->Fill(deltaSubl/GeV);
      h_delPtRel_subl_subjet->Fill(deltaSubl/ptVisSubl);

      // 2D histograms
      // true pt vs delta pt
      h2_delPt_pt_subjet->Fill(ptVisLead/GeV, deltaLead/GeV);
      h2_delPt_pt_subjet->Fill(ptVisSubl/GeV, deltaSubl/GeV);

      h2_delPt_pt_lead_subjet->Fill(ptVisLead/GeV, deltaLead/GeV);
      h2_delPt_pt_subl_subjet->Fill(ptVisSubl/GeV, deltaSubl/GeV);

      // mu vs delta pt
      h2_delPt_mu_subjet->Fill(mu, deltaLead/GeV);
      h2_delPt_mu_subjet->Fill(mu, deltaSubl/GeV);

      h2_delPt_mu_lead_subjet->Fill(mu, deltaLead/GeV);
      h2_delPt_mu_subl_subjet->Fill(mu, deltaSubl/GeV);
      h2_delPt_eta_subjet->Fill(xDiTau->subjetEta(0), deltaLead/GeV);
      h2_delPt_eta_subjet->Fill(xDiTau->subjetEta(1), deltaSubl/GeV);

      h2_fracPt_recoPt_subjet->Fill(xDiTau->subjetPt(0)/GeV, 
                                    xDiTau->subjetPt(0)/ptVisLead);
      h2_fracPt_recoPt_subjet->Fill(xDiTau->subjetPt(1)/GeV, 
                                    xDiTau->subjetPt(1)/ptVisSubl);

      // ----------------------------------------------------------------------------
      // ----------------------------------------------------------------------------

      if (nTrackLead == 1)
      {
        h2_fracPt_recoPt_subjet_1p->Fill(xDiTau->subjetPt(0)/GeV,
                                         xDiTau->subjetPt(0)/ptVisLead);
        if (fabs(tlvSubjetLead.Eta()) < 0.3)
        {
          h2_fracPt_recoPt_subjet_1p_00_03->Fill(xDiTau->subjetPt(0)/GeV,
                                                 xDiTau->subjetPt(0)/ptVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 0.3 && fabs(tlvSubjetLead.Eta()) < 0.8)
        {
          h2_fracPt_recoPt_subjet_1p_03_08->Fill(xDiTau->subjetPt(0)/GeV,
                                                 xDiTau->subjetPt(0)/ptVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 0.8 && fabs(tlvSubjetLead.Eta()) < 1.3)
        {
          h2_fracPt_recoPt_subjet_1p_08_13->Fill(xDiTau->subjetPt(0)/GeV,
                                                 xDiTau->subjetPt(0)/ptVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 1.3 && fabs(tlvSubjetLead.Eta()) < 1.6)
        {
          h2_fracPt_recoPt_subjet_1p_13_16->Fill(xDiTau->subjetPt(0)/GeV,
                                                 xDiTau->subjetPt(0)/ptVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 1.6 && fabs(tlvSubjetLead.Eta()) < 2.4)
        {
          h2_fracPt_recoPt_subjet_1p_16_24->Fill(xDiTau->subjetPt(0)/GeV,
                                                 xDiTau->subjetPt(0)/ptVisLead);
        }
      }
      if (nTrackSubl == 1)
      {
        h2_fracPt_recoPt_subjet_1p->Fill(xDiTau->subjetPt(1)/GeV,
                                         xDiTau->subjetPt(1)/ptVisSubl);

        if (fabs(tlvSubjetSubl.Eta()) < 0.3)
        {
          h2_fracPt_recoPt_subjet_1p_00_03->Fill(xDiTau->subjetPt(1)/GeV,
                                                 xDiTau->subjetPt(1)/ptVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 0.3 && fabs(tlvSubjetSubl.Eta()) < 0.8)
        {
          h2_fracPt_recoPt_subjet_1p_03_08->Fill(xDiTau->subjetPt(1)/GeV,
                                                 xDiTau->subjetPt(1)/ptVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 0.8 && fabs(tlvSubjetSubl.Eta()) < 1.3)
        {
          h2_fracPt_recoPt_subjet_1p_08_13->Fill(xDiTau->subjetPt(1)/GeV,
                                                 xDiTau->subjetPt(1)/ptVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 1.3 && fabs(tlvSubjetSubl.Eta()) < 1.6)
        {
          h2_fracPt_recoPt_subjet_1p_13_16->Fill(xDiTau->subjetPt(1)/GeV,
                                                 xDiTau->subjetPt(1)/ptVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 1.6 && fabs(tlvSubjetSubl.Eta()) < 2.4)
        {
          h2_fracPt_recoPt_subjet_1p_16_24->Fill(xDiTau->subjetPt(1)/GeV,
                                                 xDiTau->subjetPt(1)/ptVisSubl);
        }
      }
      if (nTrackLead > 1)
      {
        h2_fracPt_recoPt_subjet_mp->Fill(xDiTau->subjetPt(0)/GeV,
                                         xDiTau->subjetPt(0)/ptVisLead);

        if (fabs(tlvSubjetLead.Eta()) < 0.3)
        {
          h2_fracPt_recoPt_subjet_mp_00_03->Fill(xDiTau->subjetPt(0)/GeV,
                                                 xDiTau->subjetPt(0)/ptVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 0.3 && fabs(tlvSubjetLead.Eta()) < 0.8)
        {
          h2_fracPt_recoPt_subjet_mp_03_08->Fill(xDiTau->subjetPt(0)/GeV,
                                                 xDiTau->subjetPt(0)/ptVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 0.8 && fabs(tlvSubjetLead.Eta()) < 1.3)
        {
          h2_fracPt_recoPt_subjet_mp_08_13->Fill(xDiTau->subjetPt(0)/GeV,
                                                 xDiTau->subjetPt(0)/ptVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 1.3 && fabs(tlvSubjetLead.Eta()) < 1.6)
        {
          h2_fracPt_recoPt_subjet_mp_13_16->Fill(xDiTau->subjetPt(0)/GeV,
                                                 xDiTau->subjetPt(0)/ptVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 1.6 && fabs(tlvSubjetLead.Eta()) < 2.4)
        {
          h2_fracPt_recoPt_subjet_mp_16_24->Fill(xDiTau->subjetPt(0)/GeV,
                                                 xDiTau->subjetPt(0)/ptVisLead);
        }
      }
      if (nTrackSubl > 1)
      {
        h2_fracPt_recoPt_subjet_mp->Fill(xDiTau->subjetPt(1)/GeV,
                                         xDiTau->subjetPt(1)/ptVisSubl);

        if (fabs(tlvSubjetSubl.Eta()) < 0.3)
        {
          h2_fracPt_recoPt_subjet_mp_00_03->Fill(xDiTau->subjetPt(1)/GeV,
                                                 xDiTau->subjetPt(1)/ptVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 0.3 && fabs(tlvSubjetSubl.Eta()) < 0.8)
        {
          h2_fracPt_recoPt_subjet_mp_03_08->Fill(xDiTau->subjetPt(1)/GeV,
                                                 xDiTau->subjetPt(1)/ptVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 0.8 && fabs(tlvSubjetSubl.Eta()) < 1.3)
        {
          h2_fracPt_recoPt_subjet_mp_08_13->Fill(xDiTau->subjetPt(1)/GeV,
                                                 xDiTau->subjetPt(1)/ptVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 1.3 && fabs(tlvSubjetSubl.Eta()) < 1.6)
        {
          h2_fracPt_recoPt_subjet_mp_13_16->Fill(xDiTau->subjetPt(1)/GeV,
                                                 xDiTau->subjetPt(1)/ptVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 1.6 && fabs(tlvSubjetSubl.Eta()) < 2.4)
        {
          h2_fracPt_recoPt_subjet_mp_16_24->Fill(xDiTau->subjetPt(1)/GeV,
                                                 xDiTau->subjetPt(1)/ptVisSubl);
        }
      }

      // ----------------------------------------------------------------------------
      // ----------------------------------------------------------------------------
      if (nTrackLead == 1)
      {
        if (fabs(tlvSubjetLead.Eta()) < 0.3)
        {
          h2_relPt_pt_subjet_1p_00_03->Fill(ptVisLead/GeV, 
                                            (xDiTau->subjetPt(0) - ptVisLead)/ptVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 0.3 && fabs(tlvSubjetLead.Eta()) < 0.8)
        {
          h2_relPt_pt_subjet_1p_03_08->Fill(ptVisLead/GeV, 
                                            (xDiTau->subjetPt(0) - ptVisLead)/ptVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 0.8 && fabs(tlvSubjetLead.Eta()) < 1.3)
        {
          h2_relPt_pt_subjet_1p_08_13->Fill(ptVisLead/GeV, 
                                            (xDiTau->subjetPt(0) - ptVisLead)/ptVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 1.3 && fabs(tlvSubjetLead.Eta()) < 1.6)
        {
          h2_relPt_pt_subjet_1p_13_16->Fill(ptVisLead/GeV, 
                                            (xDiTau->subjetPt(0) - ptVisLead)/ptVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 1.6 && fabs(tlvSubjetLead.Eta()) < 2.4)
        {
          h2_relPt_pt_subjet_1p_16_24->Fill(ptVisLead/GeV, 
                                            (xDiTau->subjetPt(0) - ptVisLead)/ptVisLead);
        }
      }
      if (nTrackSubl == 1)
      {
        if (fabs(tlvSubjetSubl.Eta()) < 0.3)
        {
          h2_relPt_pt_subjet_1p_00_03->Fill(ptVisSubl/GeV,
                                            (xDiTau->subjetPt(1) - ptVisSubl)/ptVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 0.3 && fabs(tlvSubjetSubl.Eta()) < 0.8)
        {
          h2_relPt_pt_subjet_1p_03_08->Fill(ptVisSubl/GeV,
                                            (xDiTau->subjetPt(1) - ptVisSubl)/ptVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 0.8 && fabs(tlvSubjetSubl.Eta()) < 1.3)
        {
          h2_relPt_pt_subjet_1p_08_13->Fill(ptVisSubl/GeV,
                                            (xDiTau->subjetPt(1) - ptVisSubl)/ptVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 1.3 && fabs(tlvSubjetSubl.Eta()) < 1.6)
        {
          h2_relPt_pt_subjet_1p_13_16->Fill(ptVisSubl/GeV,
                                            (xDiTau->subjetPt(1) - ptVisSubl)/ptVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 1.6 && fabs(tlvSubjetLead.Eta()) < 2.4)
        {
          h2_relPt_pt_subjet_1p_16_24->Fill(ptVisSubl/GeV,
                                            (xDiTau->subjetPt(1) - ptVisSubl)/ptVisSubl);
        }
      }
      if (nTrackLead > 1)
      {
        if (fabs(tlvSubjetLead.Eta()) < 0.3)
        {
          h2_relPt_pt_subjet_mp_00_03->Fill(ptVisLead/GeV,
                                            (xDiTau->subjetPt(0) - ptVisLead)/ptVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 0.3 && fabs(tlvSubjetLead.Eta()) < 0.8)
        {
          h2_relPt_pt_subjet_mp_03_08->Fill(ptVisLead/GeV,
                                            (xDiTau->subjetPt(0) - ptVisLead)/ptVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 0.8 && fabs(tlvSubjetLead.Eta()) < 1.3)
        {
          h2_relPt_pt_subjet_mp_08_13->Fill(ptVisLead/GeV,
                                            (xDiTau->subjetPt(0) - ptVisLead)/ptVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 1.3 && fabs(tlvSubjetLead.Eta()) < 1.6)
        {
          h2_relPt_pt_subjet_mp_13_16->Fill(ptVisLead/GeV,
                                            (xDiTau->subjetPt(0) - ptVisLead)/ptVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 1.6 && fabs(tlvSubjetLead.Eta()) < 2.4)
        {
          h2_relPt_pt_subjet_mp_16_24->Fill(ptVisLead/GeV,
                                            (xDiTau->subjetPt(0) - ptVisLead)/ptVisLead);
        }
      }
      if (nTrackSubl > 1)
      {
        if (fabs(tlvSubjetSubl.Eta()) < 0.3)
        {
          h2_relPt_pt_subjet_mp_00_03->Fill(ptVisSubl/GeV,
                                            (xDiTau->subjetPt(1) - ptVisSubl)/ptVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 0.3 && fabs(tlvSubjetSubl.Eta()) < 0.8)
        {
          h2_relPt_pt_subjet_mp_03_08->Fill(ptVisSubl/GeV,
                                            (xDiTau->subjetPt(1) - ptVisSubl)/ptVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 0.8 && fabs(tlvSubjetSubl.Eta()) < 1.3)
        {
          h2_relPt_pt_subjet_mp_08_13->Fill(ptVisSubl/GeV,
                                            (xDiTau->subjetPt(1) - ptVisSubl)/ptVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 1.3 && fabs(tlvSubjetSubl.Eta()) < 1.6)
        {
          h2_relPt_pt_subjet_mp_13_16->Fill(ptVisSubl/GeV,
                                            (xDiTau->subjetPt(1) - ptVisSubl)/ptVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 1.6 && fabs(tlvSubjetLead.Eta()) < 2.4)
        {
          h2_relPt_pt_subjet_mp_16_24->Fill(ptVisSubl/GeV,
                                            (xDiTau->subjetPt(1) - ptVisSubl)/ptVisSubl);
        }
      }

      // ----------------------------------------------------------------------------
      // ----------------------------------------------------------------------------

    } // if is truth matched
  } // loop over ditaus

  // ----------------------------------------------------------------------------
  // tau Rec
  // ----------------------------------------------------------------------------
  // for (const auto * xTau: *xTauContainer)
  // {
  //   if (xTau->auxdata<char>("IsTruthMatched") == (char)true)
  //   {
  //     const xAOD::TruthParticle* xTruthTauMatch;
  //     if (xTau->p4().DeltaR(xTruthTauLead->p4()) < 0.2)
  //     {
  //       xTruthTauMatch = xTruthTauLead;
  //     }
  //     else if (xTau->p4().DeltaR(xTruthTauSubl->p4()) < 0.2)
  //     {
  //       xTruthTauMatch = xTruthTauSubl;
  //     }
  //     else
  //     {
  //       continue;
  //     }
  //     double pt_vis = xTruthTauMatch->auxdata<double>("pt_vis");
  //     double delta = xTau->pt() - pt_vis;
  //     h_delPt_tau->Fill(delta/GeV);
  //     h_delPtRel_tau->Fill(delta/pt_vis);
  //     h2_delPt_pt_tau->Fill(pt_vis/GeV, delta/GeV);
  //     h2_delPt_mu_tau->Fill(mu, delta/GeV);
  //     h2_delPtRel_eta_tau->Fill(xTau->eta(), delta/GeV);
  //   }  // if is truth matched
  // }  // loop over taus

  return EL::StatusCode::SUCCESS;
}

// ----------------------------------------------------------------------------
int DiTauEnergyScale::getNtracks (const xAOD::DiTauJet* xDiTau, int iSubjet)
{
  if (iSubjet >= xDiTau->nSubjets()) 
  {
    Warning("getNtracks()", 
      "Try to get subjet number %i but this DiTau has only %i subjets. Return 0.", 
      iSubjet, xDiTau->nSubjets());
    return 0;
  }

  if (!xDiTau->isAvailable< TrackParticleLinks_t >("trackLinks") )
  {
    Warning("getNtracks()", "Track links not available. Return 0.");
    return 0;
  } 

  int nTracks = 0;
  TrackParticleLinks_t xTracks = xDiTau->trackLinks();

  TLorentzVector tlvSubjet = TLorentzVector();
  tlvSubjet.SetPtEtaPhiE(xDiTau->subjetPt(iSubjet),
                         xDiTau->subjetEta(iSubjet),
                         xDiTau->subjetPhi(iSubjet),
                         xDiTau->subjetE(iSubjet));
  TLorentzVector tlvTrack = TLorentzVector();
  for (auto xTrack: xTracks)
  {
    tlvTrack.SetPtEtaPhiE( (*xTrack)->pt(),
                           (*xTrack)->eta(),
                           (*xTrack)->phi(),
                           (*xTrack)->e());
    if (tlvSubjet.DeltaR(tlvTrack) < xDiTau->auxdata<float>("R_subjet"))
    {
      nTracks++;
    }
  }

  return nTracks;
}


