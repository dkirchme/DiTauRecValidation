#include <DiTauRecValidation/DiTauEnergyScale.h>

#include <DiTauRecValidation/IDVariables.h>

#include <DiTauRecValidation/RecoEfficiency.h>

#include <DiTauRecValidation/ParallelDriver.h>

#include <DiTauRecValidation/TriggerAnalysis.h>


#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#pragma link C++ class RecoEfficiency+;
#pragma link C++ class IDVariables+;
#pragma link C++ class DiTauEnergyScale+;
#pragma link C++ class EL::ParallelDriver+;
#pragma link C++ class TriggerAnalysis+;

#endif

#ifdef __ROOTCLING__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;
#pragma link C++ class RecoEfficiency+;
#pragma link C++ class IDVariables+;
#pragma link C++ class DiTauEnergyScale+;
#pragma link C++ class EL::ParallelDriver+;
#pragma link C++ class TriggerAnalysis+;

#endif

