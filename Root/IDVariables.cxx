#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <DiTauRecValidation/IDVariables.h>
// #include <TSystem.h>

// EDM includes
#include "xAODEventInfo/EventInfo.h"
#include "xAODTau/DiTauJetContainer.h"
#include "xAODTruth/TruthParticleContainer.h"

// Tools
#include "TauAnalysisTools/TauTruthMatchingTool.h"
#include "DiTauID/DiTauIDVarCalculator.h"
#include "DiTauID/DiTauDiscriminant.h"

// Infrastructure includes:
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/tools/Message.h"
#include "EventLoop/OutputStream.h"
#include "AsgTools/MsgStream.h"
#include "AsgTools/MsgStreamMacros.h"
#include <AsgTools/MessageCheck.h>
#include "xAODCore/ShallowCopy.h"


// Root
#include "TFile.h"
#include "TLorentzVector.h"

typedef std::vector< ElementLink< xAOD::TrackParticleContainer > >  TrackParticleLinks_t;

// this is needed to distribute the algorithm to the workers
ClassImp(IDVariables)

/// helper macros for checking and retriving xAOD containers
#define CHECK( ARG )                    \
  do {                                                  \
    const EL::StatusCode result = ARG;                \
    if( result == EL::StatusCode::FAILURE ) {                    \
      ::Error( "IDVariables", "Failed to execute: \"%s\"",  \
           #ARG );                  \
      return 1;                     \
    }                           \
  } while( false )


// ##########################################################################
IDVariables::IDVariables ()
{
  m_IDVarCalculator = 0;
  m_T2MT = 0;
  m_DiscrTool = 0;
  m_writeToAOD = false;
}


// ##########################################################################
EL::StatusCode IDVariables::setupJob (EL::Job& job)
{
  ANA_CHECK_SET_TYPE( EL::StatusCode );
  Info("setupJob()", "setupJob");
  job.useXAOD();
  ANA_CHECK( xAOD::Init() );

  xAOD::TEvent::EAuxMode mode = xAOD::TEvent::kBranchAccess; // alternative: xAOD::TEvent::kClassAccess;
  xAOD::TEvent event(mode);

  return EL::StatusCode::SUCCESS;
}


// ##########################################################################
EL::StatusCode IDVariables::histInitialize ()
{
  // ----------------------------------------------------------------------------
  // histograms for reco efficiency 
  // ----------------------------------------------------------------------------
  h_ditau_true_pt = new TH1F("h_ditau_true_pt",
          ";true ditau p_{T} [GeV]", 200, 0, 2000);
  h_ditau_match_true_pt = new TH1F("h_ditau_match_true_pt",
          ";true ditau p_{T} [GeV]", 200, 0, 2000);
  h_ditau_true_pt_vis = new TH1F("h_ditau_true_pt_vis",
          ";true vis ditau p_{T} [GeV]", 200, 0, 2000);
  h_ditau_match_true_pt_vis = new TH1F("h_ditau_match_true_pt_vis",
          ";true vis ditau p_{T} [GeV]", 200, 0, 2000);
  h_ditau_true_dR = new TH1F("h_ditau_true_dR",
          ";#DeltaR", 100, 0, 4);
  h_ditau_match_true_dR = new TH1F("h_ditau_match_true_dR",
          ";#DeltaR", 100, 0, 4);
  h_ditau_true_mu = new TH1F("h_ditau_true_mu", 
          ";#mu", 50, 0, 50);
  h_ditau_match_true_mu = new TH1F("h_ditau_match_true_mu", 
          ";#mu", 50, 0, 50);

  // ----------------------------------------------------------------------------
  // histograms for energy differences in DiTauRec
  // ----------------------------------------------------------------------------  
  // 1 dim relative delta pt
  h_delPtRel_ditau = new TH1F("h_delPtRel_ditau", 
          ";(reco ditau p_{T} - true vis ditau p_{T}) / true vis ditau p_{T};",
          200, -1, 1);
  h_delPtRel_subjet = new TH1F("h_delPtRel_subjet", 
          ";(subjet p_{T} - true vis tau p_{T}) / true vis tau p_{T};",
          200, -1, 1);
  h_delPtRel_lead_subjet = new TH1F("h_delPtRel_lead_subjet", 
          ";(leading subjet p_{T} - true vis tau p_{T}) / true vis tau p_{T};",
          200, -1, 1);
  h_delPtRel_subl_subjet = new TH1F("h_delPtRel_subl_subjet", 
          ";(subleading subjet p_{T} - true vis tau p_{T}) / true vis tau p_{T};",
          200, -1, 1);
  
  // 2 dim fraction reco pt / true pt vs reco pt 
  h2_fracE_recoE_subjet = new TH2F("h2_fracE_recoE_subjet",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  h2_fracE_recoE_subjet_1p = new TH2F("h2_fracE_recoE_subjet_1p",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  h2_fracE_recoE_subjet_mp = new TH2F("h2_fracE_recoE_subjet_mp",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  h2_fracE_recoE_subjet_2p = new TH2F("h2_fracE_recoE_subjet_2p",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  h2_fracE_recoE_subjet_3p = new TH2F("h2_fracE_recoE_subjet_3p",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  h2_fracE_recoE_subjet_4p = new TH2F("h2_fracE_recoE_subjet_4p",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  h2_fracE_recoE_subjet_5p = new TH2F("h2_fracE_recoE_subjet_5p",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  // 2 dim fraction reco pt / true pt vs reco pt for different eta
  h2_fracE_recoE_subjet_1p_00_03 = new TH2F("h2_fracE_recoE_subjet_1p_00_03",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  h2_fracE_recoE_subjet_1p_03_08 = new TH2F("h2_fracE_recoE_subjet_1p_03_08",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  h2_fracE_recoE_subjet_1p_08_13 = new TH2F("h2_fracE_recoE_subjet_1p_08_13",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  h2_fracE_recoE_subjet_1p_13_16 = new TH2F("h2_fracE_recoE_subjet_1p_13_16",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  h2_fracE_recoE_subjet_1p_16_24 = new TH2F("h2_fracE_recoE_subjet_1p_16_24",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  h2_fracE_recoE_subjet_mp_00_03 = new TH2F("h2_fracE_recoE_subjet_mp_00_03",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  h2_fracE_recoE_subjet_mp_03_08 = new TH2F("h2_fracE_recoE_subjet_mp_03_08",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  h2_fracE_recoE_subjet_mp_08_13 = new TH2F("h2_fracE_recoE_subjet_mp_08_13",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  h2_fracE_recoE_subjet_mp_13_16 = new TH2F("h2_fracE_recoE_subjet_mp_13_16",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);
  h2_fracE_recoE_subjet_mp_16_24 = new TH2F("h2_fracE_recoE_subjet_mp_16_24",
          ";p_{T,LC}^{#tau} [GeV];p_{T,LC}^{#tau}/p_{T,vis}^{true}",
          20, 0, 1000, 40, 0.5, 1.5);

  // 2 dim relative delta pt vs true pt for different eta
  h2_relE_E_subjet_1p_00_03 = new TH2F("h2_relE_E_subjet_1p_00_03",
      ";p_{T,vis}^{true} [GeV];(p_{T,LC}^{#tau} - p_{T,vis}^{true}) / p_{T,vis}^{true};", 
          20, 0, 1000, 20, -1, 1);
  h2_relE_E_subjet_1p_03_08 = new TH2F("h2_relE_E_subjet_1p_03_08",
      ";p_{T,vis}^{true} [GeV];(p_{T,LC}^{#tau} - p_{T,vis}^{true}) / p_{T,vis}^{true};", 
          20, 0, 1000, 20, -1, 1);
  h2_relE_E_subjet_1p_08_13 = new TH2F("h2_relE_E_subjet_1p_08_13",
      ";p_{T,vis}^{true} [GeV];(p_{T,LC}^{#tau} - p_{T,vis}^{true}) / p_{T,vis}^{true};", 
          20, 0, 1000, 20, -1, 1);
  h2_relE_E_subjet_1p_13_16 = new TH2F("h2_relE_E_subjet_1p_13_16",
      ";p_{T,vis}^{true} [GeV];(p_{T,LC}^{#tau} - p_{T,vis}^{true}) / p_{T,vis}^{true};", 
          20, 0, 1000, 20, -1, 1);
  h2_relE_E_subjet_1p_16_24 = new TH2F("h2_relE_E_subjet_1p_16_24",
      ";p_{T,vis}^{true} [GeV];(p_{T,LC}^{#tau} - p_{T,vis}^{true}) / p_{T,vis}^{true};", 
          20, 0, 1000, 20, -1, 1);
  h2_relE_E_subjet_mp_00_03 = new TH2F("h2_relE_E_subjet_mp_00_03",
      ";p_{T,vis}^{true} [GeV];(p_{T,LC}^{#tau} - p_{T,vis}^{true}) / p_{T,vis}^{true};", 
          20, 0, 1000, 20, -1, 1);
  h2_relE_E_subjet_mp_03_08 = new TH2F("h2_relE_E_subjet_mp_03_08",
      ";p_{T,vis}^{true} [GeV];(p_{T,LC}^{#tau} - p_{T,vis}^{true}) / p_{T,vis}^{true};", 
          20, 0, 1000, 20, -1, 1);
  h2_relE_E_subjet_mp_08_13 = new TH2F("h2_relE_E_subjet_mp_08_13",
      ";p_{T,vis}^{true} [GeV];(p_{T,LC}^{#tau} - p_{T,vis}^{true}) / p_{T,vis}^{true};", 
          20, 0, 1000, 20, -1, 1);
  h2_relE_E_subjet_mp_13_16 = new TH2F("h2_relE_E_subjet_mp_13_16",
      ";p_{T,vis}^{true} [GeV];(p_{T,LC}^{#tau} - p_{T,vis}^{true}) / p_{T,vis}^{true};", 
          20, 0, 1000, 20, -1, 1);
  h2_relE_E_subjet_mp_16_24 = new TH2F("h2_relE_E_subjet_mp_16_24",
      ";p_{T,vis}^{true} [GeV];(p_{T,LC}^{#tau} - p_{T,vis}^{true}) / p_{T,vis}^{true};", 
          20, 0, 1000, 20, -1, 1);


  // ----------------------------------------------------------------------------
  // add histupgrams to worker
  // ----------------------------------------------------------------------------
  // histograms for reco efficiency
  wk()->addOutput(h_ditau_true_pt);
  wk()->addOutput(h_ditau_match_true_pt);
  wk()->addOutput(h_ditau_true_pt_vis);
  wk()->addOutput(h_ditau_match_true_pt_vis);
  wk()->addOutput(h_ditau_true_dR);
  wk()->addOutput(h_ditau_match_true_dR);
  wk()->addOutput(h_ditau_true_mu);
  wk()->addOutput(h_ditau_match_true_mu);

  // histograms for energy differences in DiTauRec
  wk()->addOutput(h_delPtRel_ditau);
  wk()->addOutput(h_delPtRel_subjet);
  wk()->addOutput(h_delPtRel_lead_subjet);
  wk()->addOutput(h_delPtRel_subl_subjet);
  
  wk()->addOutput(h2_fracE_recoE_subjet);
  wk()->addOutput(h2_fracE_recoE_subjet_1p);
  wk()->addOutput(h2_fracE_recoE_subjet_mp);
  wk()->addOutput(h2_fracE_recoE_subjet_2p);
  wk()->addOutput(h2_fracE_recoE_subjet_3p);
  wk()->addOutput(h2_fracE_recoE_subjet_4p);
  wk()->addOutput(h2_fracE_recoE_subjet_5p);
  wk()->addOutput(h2_fracE_recoE_subjet_1p_00_03);
  wk()->addOutput(h2_fracE_recoE_subjet_1p_03_08);
  wk()->addOutput(h2_fracE_recoE_subjet_1p_08_13);
  wk()->addOutput(h2_fracE_recoE_subjet_1p_13_16);
  wk()->addOutput(h2_fracE_recoE_subjet_1p_16_24);
  wk()->addOutput(h2_fracE_recoE_subjet_mp_00_03);
  wk()->addOutput(h2_fracE_recoE_subjet_mp_03_08);
  wk()->addOutput(h2_fracE_recoE_subjet_mp_08_13);
  wk()->addOutput(h2_fracE_recoE_subjet_mp_13_16);
  wk()->addOutput(h2_fracE_recoE_subjet_mp_16_24);

  wk()->addOutput(h2_relE_E_subjet_1p_00_03);
  wk()->addOutput(h2_relE_E_subjet_1p_03_08);
  wk()->addOutput(h2_relE_E_subjet_1p_08_13);
  wk()->addOutput(h2_relE_E_subjet_1p_13_16);
  wk()->addOutput(h2_relE_E_subjet_1p_16_24);
  wk()->addOutput(h2_relE_E_subjet_mp_00_03);
  wk()->addOutput(h2_relE_E_subjet_mp_03_08);
  wk()->addOutput(h2_relE_E_subjet_mp_08_13);
  wk()->addOutput(h2_relE_E_subjet_mp_13_16);
  wk()->addOutput(h2_relE_E_subjet_mp_16_24);
  
  return EL::StatusCode::SUCCESS;
}

// ##########################################################################
EL::StatusCode IDVariables::fileExecute ()
{
  return EL::StatusCode::SUCCESS;
}



// ##########################################################################
EL::StatusCode IDVariables::changeInput (bool firstFile)
{
  ANA_CHECK_SET_TYPE( EL::StatusCode );

  xAOD::TEvent* event = wk()->xaodEvent();
  
  const xAOD::EventInfo* eventInfo = 0;
  ANA_CHECK( event->retrieve( eventInfo, "EventInfo") );

  isMC = false;
  if (eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION)) isMC = true;

  return EL::StatusCode::SUCCESS;
}



// ##########################################################################
EL::StatusCode IDVariables::initialize ()
{
  ANA_CHECK_SET_TYPE( EL::StatusCode );

  Info("initialize()", "initialize");


  xAOD::TEvent* event = wk()->xaodEvent();

  m_evtSelector = new EventSelector("evtselector_for_idvariables");
  ANA_CHECK(m_evtSelector->initialize());

  // write to output xAOD
  if (m_writeToAOD)
  {
    TFile *file = wk()->getOutputFile("AOD");
    ANA_CHECK( event->writeTo(file) );
  }  

  // initialize tau calibration tool
  m_tauCalibTool = new TauCalibrateLC("TauCalibrateLC");
  // m_tauCalibTool->msg().setLevel(MSG::DEBUG);
  // ANA_CHECK( m_tauCalibTool->setProperty("calibrationFile", "TES_Rel20.root") );
  ANA_CHECK( m_tauCalibTool->setProperty("calibrationFile", "TES_MC15.root") );
  ANA_CHECK( m_tauCalibTool->setProperty("doEnergyCorrection", true) );
  // ANA_CHECK( m_tauCalibTool->setProperty("doAxisCorrection", true) );
  ANA_CHECK( m_tauCalibTool->initialize() );

  // initialize tool to calculate ID variables
  m_IDVarCalculator = new DiTauID::DiTauIDVarCalculator("IDVarCalculator");
  m_IDVarCalculator->msg().setLevel(MSG::INFO);
  ANA_CHECK( m_IDVarCalculator->setProperty("doCalcCluserVariables", false) );
  ANA_CHECK( m_IDVarCalculator->initialize() );

  // initialize truth matching tool
  if (isMC)
  {
    m_T2MT = new TauAnalysisTools::TauTruthMatchingTool("TauTruthMatchingTool");
    m_T2MT->msg().setLevel(MSG::WARNING);
    ANA_CHECK( m_T2MT->setProperty("WriteTruthTaus", true) );
    ANA_CHECK( m_T2MT->initialize() );
  }

  // TODO: make this a property of IDVariables()
  std::string weightdir = getenv("ROOTCOREBIN");
  weightdir += "/data/DiTauID/";
  // std::string sWeightsFile = weightdir + "TMVAClassification_BDT.weights.xml";
  std::string sWeightsFile = weightdir + "TMVAClassification_BDT_logAbsS_logMtrack_logFiso.weights.xml";
  
  // initialize ditau discriminant
  m_DiscrTool = new DiTauID::DiTauDiscriminant("DiTauDiscriminant");
  // m_DiscrTool->msg().setLevel(MSG::DEBUG);
  ANA_CHECK( m_DiscrTool->setProperty("WeightsFile", sWeightsFile) );
  ANA_CHECK( m_DiscrTool->initialize() );

  return EL::StatusCode::SUCCESS;
}



// ##########################################################################
EL::StatusCode IDVariables::execute ()
{
  ANA_CHECK_SET_TYPE( EL::StatusCode );

  // ----------------------------------------------------------------------------
  // event infos 
  // ----------------------------------------------------------------------------
  xAOD::TEvent* event = wk()->xaodEvent();

  const xAOD::EventInfo* eventInfo = 0;
  ANA_CHECK( event->retrieve( eventInfo, "EventInfo") );

  // ----------------------------------------------------------------------------
  // execute tools
  // ----------------------------------------------------------------------------

  // good run list
  if (! m_evtSelector->checkEvent(event))
  {
    return EL::StatusCode::SUCCESS;
  }

  // initialze ditau variable calculator for this event
  ANA_CHECK( m_IDVarCalculator->initializeEvent() );

  // create TruthTau Container in output file
  if (isMC)
  {
    ANA_CHECK( m_T2MT->initializeEvent() );
  }

  // get ditau container
  const xAOD::DiTauJetContainer* xDiTauContainer = 0;
  ANA_CHECK( event->retrieve(xDiTauContainer, "DiTauJets") );

  // get tau container
  const xAOD::TauJetContainer* xTauContainer = 0;
  ANA_CHECK( event->retrieve(xTauContainer, "TauJets") );

  // make shallow copy
  std::pair< xAOD::DiTauJetContainer*, xAOD::ShallowAuxContainer* >xDiTauShallowCont = xAOD::shallowCopyContainer(*xDiTauContainer);

  // loop over ditau candidates
  double iBDTScore;
  for (const auto xDiTau: *xDiTauContainer) 
  {
    if (isMC)
    {
      auto xTruthDiTau = m_T2MT->getTruth(*xDiTau);
    }
    // ANA_CHECK( decorNtracks(*xDiTau) );
    ANA_CHECK( m_IDVarCalculator->calculateIDVariables(*xDiTau) );
    iBDTScore = m_DiscrTool->getJetBDTScore(*xDiTau);
  }
  for (const auto xTau: *xTauContainer)
  {
    if (isMC)
    {
      auto xTruthTau = m_T2MT->getTruth(*xTau);
    }
  }


  // TES
  for (const auto xDiTau: *xDiTauShallowCont.first)
  {
    ANA_CHECK( m_tauCalibTool->execute(*xDiTau) );
  }

  // CHECK(plotDTES(event, xDiTauContainer, xTauContainer));
  if (isMC)
  {
    CHECK(plotDTES(event, xDiTauShallowCont.first, xTauContainer));
  }


  if (isMC)
  {
    for (const auto xDiTau: *xDiTauContainer)
    {
      if (!(bool)xDiTau->auxdata<char>("IsTruthMatched"))
        continue;
      auto truthTaus = xDiTau->auxdata<std::vector<ElementLink<xAOD::TruthParticleContainer>>>("TruthTaus");
      // Info("execute()", "truth tau 0 eta: %.2f phi %.2f", (*truthTaus.at(0))->eta(), (*truthTaus.at(0))->phi());
      // Info("execute()", "truth tau 1 eta: %.2f phi %.2f", (*truthTaus.at(1))->eta(), (*truthTaus.at(1))->phi());
      // Info("execute()", "lead subjet eta: %.2f phi %.2f", xDiTau->subjetEta(0), xDiTau->subjetPhi(0));
      // Info("execute()", "subl subjet eta: %.2f phi %.2f", xDiTau->subjetEta(1), xDiTau->subjetPhi(1));
    }
  }



  // ----------------------------------------------------------------------------
  // fill event in xAOD output
  // ----------------------------------------------------------------------------
  if (m_writeToAOD)
  {
    xDiTauShallowCont.second->setShallowIO(false);
    ANA_CHECK( event->record(xDiTauShallowCont.first, "_DiTauJets") );
    ANA_CHECK( event->record(xDiTauShallowCont.second, "_DiTauJetsAux.") );

    ANA_CHECK( event->copy("DiTauJets") );
    if (isMC)
    {
      ANA_CHECK( event->copy("TruthParticles") );
    }
    ANA_CHECK( event->copy("TauJets") );
    // ANA_CHECK( event->copy("InDetTrackParticles") );
    ANA_CHECK( event->copy("AntiKt10LCTopoJets") );
    ANA_CHECK( event->copy("EventInfo") );
    event->fill();
  }

  xAOD::TStore* store = wk()->xaodStore();
  ANA_CHECK(store->record(xDiTauShallowCont.first, "_DiTauJets"));
  ANA_CHECK(store->record(xDiTauShallowCont.second, "_DiTauJetsAux."));

  return EL::StatusCode::SUCCESS;
}


// ##########################################################################
EL::StatusCode IDVariables::postExecute ()
{
  return EL::StatusCode::SUCCESS;
}



// ##########################################################################
EL::StatusCode IDVariables::finalize ()
{
  ANA_CHECK_SET_TYPE( EL::StatusCode );

  Info("finalize()", "finalize()");
  xAOD::TEvent* event = wk()->xaodEvent();

  if (m_writeToAOD)
  {
    TFile *file = wk()->getOutputFile("AOD");
    ANA_CHECK( event->finishWritingTo(file) );
  }

  if (m_evtSelector)
  {
    delete m_evtSelector;
    m_evtSelector = 0;
  }
  if (m_IDVarCalculator)
  {
    delete m_IDVarCalculator;
    m_IDVarCalculator = 0;
  }
  if (m_T2MT)
  {
    delete m_T2MT;
    m_T2MT = 0;
  }
  if (m_DiscrTool)
  {
    delete m_DiscrTool;
    m_DiscrTool = 0;
  }

  return EL::StatusCode::SUCCESS;
}


// ##########################################################################
EL::StatusCode IDVariables::histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.

  return EL::StatusCode::SUCCESS;
}


// ##########################################################################
EL::StatusCode IDVariables::writeToAOD(bool bWriteToAOD)
{
  m_writeToAOD = bWriteToAOD;
  return EL::StatusCode::SUCCESS;
}


// ##########################################################################
// EL::StatusCode IDVariables::decorNtracks (const xAOD::DiTauJet& xDiTau)
// {
//   if (!xDiTau.isAvailable< TrackParticleLinks_t >("trackLinks") )
//   {
//     Warning("decorNtracks()", "Track links not available.");
//     return StatusCode::FAILURE;
//   } 

//   std::vector<int> nTracks;
//   std::vector<int> nCoreTracks;
//   int nSubjets = xDiTau.nSubjets();

//   for (int i = 0; i < nSubjets; ++i)
//   {
//     TrackParticleLinks_t xTracks = xDiTau.trackLinks();

//     TLorentzVector tlvSubjet = TLorentzVector();
//     tlvSubjet.SetPtEtaPhiE(xDiTau.subjetPt(i),
//                            xDiTau.subjetEta(i),
//                            xDiTau.subjetPhi(i),
//                            xDiTau.subjetE(i));
//     int n = 0;;;;
//     int n_core = 0;
//     for (const auto xTrack: xTracks)
//     {
//       TLorentzVector tlvTrack = (*xTrack)->p4();
//       double dR = tlvSubjet.DeltaR(tlvTrack);
//       if ( dR < xDiTau.auxdata<float>("R_subjet") ) n++;
//       if ( dR < xDiTau.auxdata<float>("R_core") ) n_core++;
//     } 

//     nTracks.push_back(n);
//     nCoreTracks.push_back(n_core);

//   } // loop over subjets

//   xDiTau.auxdecor< std::vector<int> >( "n_tracks" ) = nTracks;
//   xDiTau.auxdecor< std::vector<int> >( "n_core_tracks" ) = nTracks;

//   return StatusCode::SUCCESS;
// }



// ##########################################################################
EL::StatusCode IDVariables::plotDTES (xAOD::TEvent* event,
                const xAOD::DiTauJetContainer* xDiTauContainer,
                const xAOD::TauJetContainer* xTauContainer)
{

  // get EventInfo and pile up
  const xAOD::EventInfo* eventInfo = 0;
  ANA_CHECK(  event->retrieve( eventInfo, "EventInfo") );
  float mu = eventInfo->actualInteractionsPerCrossing();

  // get TruthTau container
  const xAOD::TruthParticleContainer* xTruthTauContainer = 0;
  ANA_CHECK( event->retrieve(xTruthTauContainer, "TruthTaus") );

  // get 2 leading truth taus
  const xAOD::TruthParticle* xTruthTauLead = 0;
  const xAOD::TruthParticle* xTruthTauSubl = 0;
  // ----------------------------------------------------------------------------
  // TODO: put in extra method; pass xTruthTauLead and xTruthTauSubl as reference
  for (const auto* xTruthTau: *xTruthTauContainer)
  {
    if ( !xTruthTauLead || xTruthTauLead->pt() < xTruthTau->pt() ) 
    {
      xTruthTauSubl = xTruthTauLead;
      xTruthTauLead = xTruthTau;
      continue;
    }
    if ( !xTruthTauSubl || xTruthTauSubl->pt() < xTruthTau->pt() )
    {
      xTruthTauSubl = xTruthTau;
    }
  }

  if ( !xTruthTauLead || !xTruthTauSubl )
  {
    return EL::StatusCode::SUCCESS;
  }
  // ----------------------------------------------------------------------------

  double true_ditau_pt = ( xTruthTauLead->p4()+xTruthTauSubl->p4() ).Pt();
  double true_ditau_dR = ( xTruthTauLead->p4().DeltaR(xTruthTauSubl->p4()) );

  double true_ditau_pt_vis = xTruthTauLead->auxdata<double>("pt_vis")
                             + xTruthTauSubl->auxdata<double>("pt_vis");

  h_ditau_true_pt->Fill(true_ditau_pt/GeV);
  h_ditau_true_dR->Fill(true_ditau_dR);
  h_ditau_true_pt_vis->Fill(true_ditau_pt_vis/GeV);
  h_ditau_true_mu->Fill(mu);

  // ----------------------------------------------------------------------------
  // DiTauRec
  // ----------------------------------------------------------------------------
  for (const auto* xDiTau: *xDiTauContainer)
  {
    if (!xDiTau->isAvailable<char>("IsTruthMatched"))
    {
      Warning("plotEnergyDiff()", "truth match info not available");
      continue;
    }

    if (xDiTau->auxdata<char>("IsTruthMatched") == (char)true) 
    {
      h_ditau_match_true_pt->Fill(true_ditau_pt/GeV);
      h_ditau_match_true_dR->Fill(true_ditau_dR);
      h_ditau_match_true_pt_vis->Fill(true_ditau_pt_vis/GeV);
      h_ditau_match_true_mu->Fill(mu);
        
      // diff between true ditau pt and reco pt
      double delta = xDiTau->auxdata<double>("ditau_pt") - true_ditau_pt_vis;
      h_delPtRel_ditau->Fill(delta/true_ditau_pt_vis);

      // ----------------------------------------------------------------------------
      // ----------------------------------------------------------------------------
      // switch, according to reconstructed ditau subjets
      // TODO: not needed if implemented in TruthMatchingTool

      TLorentzVector tlvSubjetLead = TLorentzVector();
      TLorentzVector tlvSubjetSubl = TLorentzVector();

      tlvSubjetLead.SetPtEtaPhiE(xDiTau->subjetPt(0),
                                 xDiTau->subjetEta(0),
                                 xDiTau->subjetPhi(0),
                                 xDiTau->subjetE(0));
      tlvSubjetSubl.SetPtEtaPhiE(xDiTau->subjetPt(1),
                                 xDiTau->subjetEta(1),
                                 xDiTau->subjetPhi(1),
                                 xDiTau->subjetE(1));

      // int nTrackLead = getNtracks(xDiTau, 0);
      // int nTrackSubl = getNtracks(xDiTau, 1);
      int nTrackLead = xDiTau->auxdata<std::vector<int>>("n_tracks").at(0);
      int nTrackSubl = xDiTau->auxdata<std::vector<int>>("n_tracks").at(1);

      if ( (xTruthTauLead->p4().DeltaR(tlvSubjetSubl) < 0.2) 
          && (xTruthTauSubl->p4().DeltaR(tlvSubjetLead) < 0.2) )
      {
        const xAOD::TruthParticle* xTmp;
        xTmp = xTruthTauLead;
        xTruthTauLead = xTruthTauSubl;
        xTruthTauSubl = xTmp;

        int iTmp = nTrackLead;
        nTrackLead = nTrackSubl;
        nTrackSubl = iTmp;
      }

      // ----------------------------------------------------------------------------
      // ----------------------------------------------------------------------------
      double ptVisLead = xTruthTauLead->auxdata<double>("pt_vis");
      double ptVisSubl = xTruthTauSubl->auxdata<double>("pt_vis");
      size_t nTrackTruthLead = xTruthTauLead->auxdata<size_t>("numCharged");
      size_t nTrackTruthSubl = xTruthTauSubl->auxdata<size_t>("numCharged");

      TLorentzVector* tlvVisLead = new TLorentzVector();
      TLorentzVector* tlvVisSubl = new TLorentzVector();
      tlvVisLead->SetPtEtaPhiM( xTruthTauLead->auxdata<double>("pt_vis"),
                                xTruthTauLead->auxdata<double>("eta_vis"),
                                xTruthTauLead->auxdata<double>("phi_vis"),
                                xTruthTauLead->auxdata<double>("m_vis") );
      tlvVisSubl->SetPtEtaPhiM( xTruthTauSubl->auxdata<double>("pt_vis"),
                                xTruthTauSubl->auxdata<double>("eta_vis"),
                                xTruthTauSubl->auxdata<double>("phi_vis"),
                                xTruthTauSubl->auxdata<double>("m_vis") );
      double EVisLead = tlvVisLead->E();
      double EVisSubl = tlvVisSubl->E();


      double deltaLead = xDiTau->subjetPt(0) - ptVisLead;
      double deltaSubl = xDiTau->subjetPt(1) - ptVisSubl;

      h_delPtRel_subjet->Fill(deltaLead/ptVisLead);
      h_delPtRel_subjet->Fill(deltaSubl/ptVisSubl);

      h_delPtRel_lead_subjet->Fill(deltaLead/ptVisLead);
      h_delPtRel_subl_subjet->Fill(deltaSubl/ptVisSubl);

      // 2D histograms

      // if (nTrackTruthLead == 1 || nTrackTruthLead == 3)
      // {
        h2_fracE_recoE_subjet->Fill(xDiTau->subjetE(0)/GeV, 
                                      xDiTau->subjetE(0)/EVisLead);
      // }
      // if (nTrackTruthSubl == 1 || nTrackTruthSubl ==3)
      // {
        h2_fracE_recoE_subjet->Fill(xDiTau->subjetE(1)/GeV, 
                                      xDiTau->subjetE(1)/EVisSubl);
      // }
      // ----------------------------------------------------------------------------
      // ----------------------------------------------------------------------------

      // if (nTrackLead == 1 && nTrackTruthLead == 1 )
      if (nTrackLead == 1)
      {
        h2_fracE_recoE_subjet_1p->Fill(xDiTau->subjetE(0)/GeV,
                                         xDiTau->subjetE(0)/EVisLead);
        if (fabs(tlvSubjetLead.Eta()) < 0.3)
        {
          h2_fracE_recoE_subjet_1p_00_03->Fill(xDiTau->subjetE(0)/GeV,
                                                 xDiTau->subjetE(0)/EVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 0.3 && fabs(tlvSubjetLead.Eta()) < 0.8)
        {
          h2_fracE_recoE_subjet_1p_03_08->Fill(xDiTau->subjetE(0)/GeV,
                                                 xDiTau->subjetE(0)/EVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 0.8 && fabs(tlvSubjetLead.Eta()) < 1.3)
        {
          h2_fracE_recoE_subjet_1p_08_13->Fill(xDiTau->subjetE(0)/GeV,
                                                 xDiTau->subjetE(0)/EVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 1.3 && fabs(tlvSubjetLead.Eta()) < 1.6)
        {
          h2_fracE_recoE_subjet_1p_13_16->Fill(xDiTau->subjetE(0)/GeV,
                                                 xDiTau->subjetE(0)/EVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 1.6 && fabs(tlvSubjetLead.Eta()) < 2.4)
        {
          h2_fracE_recoE_subjet_1p_16_24->Fill(xDiTau->subjetE(0)/GeV,
                                                 xDiTau->subjetE(0)/EVisLead);
        }
      }
      // if (nTrackSubl == 1 && nTrackTruthSubl == 1)
      if (nTrackSubl == 1 )
      {
        h2_fracE_recoE_subjet_1p->Fill(xDiTau->subjetE(1)/GeV,
                                         xDiTau->subjetE(1)/EVisSubl);

        if (fabs(tlvSubjetSubl.Eta()) < 0.3)
        {
          h2_fracE_recoE_subjet_1p_00_03->Fill(xDiTau->subjetE(1)/GeV,
                                                 xDiTau->subjetE(1)/EVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 0.3 && fabs(tlvSubjetSubl.Eta()) < 0.8)
        {
          h2_fracE_recoE_subjet_1p_03_08->Fill(xDiTau->subjetE(1)/GeV,
                                                 xDiTau->subjetE(1)/EVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 0.8 && fabs(tlvSubjetSubl.Eta()) < 1.3)
        {
          h2_fracE_recoE_subjet_1p_08_13->Fill(xDiTau->subjetE(1)/GeV,
                                                 xDiTau->subjetE(1)/EVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 1.3 && fabs(tlvSubjetSubl.Eta()) < 1.6)
        {
          h2_fracE_recoE_subjet_1p_13_16->Fill(xDiTau->subjetE(1)/GeV,
                                                 xDiTau->subjetE(1)/EVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 1.6 && fabs(tlvSubjetSubl.Eta()) < 2.4)
        {
          h2_fracE_recoE_subjet_1p_16_24->Fill(xDiTau->subjetE(1)/GeV,
                                                 xDiTau->subjetE(1)/EVisSubl);
        }
      }
      // if (nTrackLead > 1 && nTrackTruthLead > 1)
      if (nTrackLead > 1)
      {
        h2_fracE_recoE_subjet_mp->Fill(xDiTau->subjetE(0)/GeV,
                                         xDiTau->subjetE(0)/EVisLead);
        if (nTrackLead == 2 && nTrackTruthLead == 2)
        {
          h2_fracE_recoE_subjet_2p->Fill(xDiTau->subjetE(0)/GeV,
                                         xDiTau->subjetE(0)/EVisLead);
        }
        if (nTrackLead == 3 && nTrackTruthLead == 3)
        {
          h2_fracE_recoE_subjet_3p->Fill(xDiTau->subjetE(0)/GeV,
                                         xDiTau->subjetE(0)/EVisLead);
        }
        if (nTrackLead == 4 && nTrackTruthLead == 4)
        {
          h2_fracE_recoE_subjet_4p->Fill(xDiTau->subjetE(0)/GeV,
                                         xDiTau->subjetE(0)/EVisLead);
        }
        if (nTrackLead == 5 && nTrackTruthLead == 5)
        {
          h2_fracE_recoE_subjet_5p->Fill(xDiTau->subjetE(0)/GeV,
                                         xDiTau->subjetE(0)/EVisLead);
        }

        if (fabs(tlvSubjetLead.Eta()) < 0.3)
        {
          h2_fracE_recoE_subjet_mp_00_03->Fill(xDiTau->subjetE(0)/GeV,
                                                 xDiTau->subjetE(0)/EVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 0.3 && fabs(tlvSubjetLead.Eta()) < 0.8)
        {
          h2_fracE_recoE_subjet_mp_03_08->Fill(xDiTau->subjetE(0)/GeV,
                                                 xDiTau->subjetE(0)/EVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 0.8 && fabs(tlvSubjetLead.Eta()) < 1.3)
        {
          h2_fracE_recoE_subjet_mp_08_13->Fill(xDiTau->subjetE(0)/GeV,
                                                 xDiTau->subjetE(0)/EVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 1.3 && fabs(tlvSubjetLead.Eta()) < 1.6)
        {
          h2_fracE_recoE_subjet_mp_13_16->Fill(xDiTau->subjetE(0)/GeV,
                                                 xDiTau->subjetE(0)/EVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 1.6 && fabs(tlvSubjetLead.Eta()) < 2.4)
        {
          h2_fracE_recoE_subjet_mp_16_24->Fill(xDiTau->subjetE(0)/GeV,
                                                 xDiTau->subjetE(0)/EVisLead);
        }
      }
      // if (nTrackSubl > 1 && nTrackTruthSubl > 1)
      if (nTrackSubl > 1)
      {
        h2_fracE_recoE_subjet_mp->Fill(xDiTau->subjetE(1)/GeV,
                                         xDiTau->subjetE(1)/EVisSubl);
        if (nTrackSubl == 2 && nTrackTruthSubl == 2)
        {
        h2_fracE_recoE_subjet_4p->Fill(xDiTau->subjetE(1)/GeV,
                                         xDiTau->subjetE(1)/EVisSubl);
        }
        if (nTrackSubl == 3 && nTrackTruthSubl == 3)
        {
          h2_fracE_recoE_subjet_3p->Fill(xDiTau->subjetE(1)/GeV,
                                           xDiTau->subjetE(1)/EVisSubl);
        }
        if (nTrackSubl == 4 && nTrackTruthSubl == 4)
        {
          h2_fracE_recoE_subjet_2p->Fill(xDiTau->subjetE(1)/GeV,
                                           xDiTau->subjetE(1)/EVisSubl);
        }
        if (nTrackSubl == 5 && nTrackTruthSubl == 5)
        {
          h2_fracE_recoE_subjet_2p->Fill(xDiTau->subjetE(1)/GeV,
                                           xDiTau->subjetE(1)/EVisSubl);
        }

        if (fabs(tlvSubjetSubl.Eta()) < 0.3)
        {
          h2_fracE_recoE_subjet_mp_00_03->Fill(xDiTau->subjetE(1)/GeV,
                                                 xDiTau->subjetE(1)/EVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 0.3 && fabs(tlvSubjetSubl.Eta()) < 0.8)
        {
          h2_fracE_recoE_subjet_mp_03_08->Fill(xDiTau->subjetE(1)/GeV,
                                                 xDiTau->subjetE(1)/EVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 0.8 && fabs(tlvSubjetSubl.Eta()) < 1.3)
        {
          h2_fracE_recoE_subjet_mp_08_13->Fill(xDiTau->subjetE(1)/GeV,
                                                 xDiTau->subjetE(1)/EVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 1.3 && fabs(tlvSubjetSubl.Eta()) < 1.6)
        {
          h2_fracE_recoE_subjet_mp_13_16->Fill(xDiTau->subjetE(1)/GeV,
                                                 xDiTau->subjetE(1)/EVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 1.6 && fabs(tlvSubjetSubl.Eta()) < 2.4)
        {
          h2_fracE_recoE_subjet_mp_16_24->Fill(xDiTau->subjetE(1)/GeV,
                                                 xDiTau->subjetE(1)/EVisSubl);
        }
      }


      // ----------------------------------------------------------------------------
      // ----------------------------------------------------------------------------
      // if (nTrackLead == 1 && nTrackTruthLead == 1)
      if (nTrackLead == 1)
      {
        if (fabs(tlvSubjetLead.Eta()) < 0.3)
        {
          h2_relE_E_subjet_1p_00_03->Fill(EVisLead/GeV, 
                                            (xDiTau->subjetE(0) - EVisLead)/EVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 0.3 && fabs(tlvSubjetLead.Eta()) < 0.8)
        {
          h2_relE_E_subjet_1p_03_08->Fill(EVisLead/GeV, 
                                            (xDiTau->subjetE(0) - EVisLead)/EVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 0.8 && fabs(tlvSubjetLead.Eta()) < 1.3)
        {
          h2_relE_E_subjet_1p_08_13->Fill(EVisLead/GeV, 
                                            (xDiTau->subjetE(0) - EVisLead)/EVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 1.3 && fabs(tlvSubjetLead.Eta()) < 1.6)
        {
          h2_relE_E_subjet_1p_13_16->Fill(EVisLead/GeV, 
                                            (xDiTau->subjetE(0) - EVisLead)/EVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 1.6 && fabs(tlvSubjetLead.Eta()) < 2.4)
        {
          h2_relE_E_subjet_1p_16_24->Fill(EVisLead/GeV, 
                                            (xDiTau->subjetE(0) - EVisLead)/EVisLead);
        }
      }
      // if (nTrackSubl == 1 && nTrackTruthSubl == 1)
      if (nTrackSubl == 1)
      {
        if (fabs(tlvSubjetSubl.Eta()) < 0.3)
        {
          h2_relE_E_subjet_1p_00_03->Fill(EVisSubl/GeV,
                                            (xDiTau->subjetE(1) - EVisSubl)/EVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 0.3 && fabs(tlvSubjetSubl.Eta()) < 0.8)
        {
          h2_relE_E_subjet_1p_03_08->Fill(EVisSubl/GeV,
                                            (xDiTau->subjetE(1) - EVisSubl)/EVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 0.8 && fabs(tlvSubjetSubl.Eta()) < 1.3)
        {
          h2_relE_E_subjet_1p_08_13->Fill(EVisSubl/GeV,
                                            (xDiTau->subjetE(1) - EVisSubl)/EVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 1.3 && fabs(tlvSubjetSubl.Eta()) < 1.6)
        {
          h2_relE_E_subjet_1p_13_16->Fill(EVisSubl/GeV,
                                            (xDiTau->subjetE(1) - EVisSubl)/EVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 1.6 && fabs(tlvSubjetLead.Eta()) < 2.4)
        {
          h2_relE_E_subjet_1p_16_24->Fill(EVisSubl/GeV,
                                            (xDiTau->subjetE(1) - EVisSubl)/EVisSubl);
        }
      }
      // if (nTrackLead > 1 && nTrackTruthLead > 1)
      if (nTrackLead > 1)
      {
        if (fabs(tlvSubjetLead.Eta()) < 0.3)
        {
          h2_relE_E_subjet_mp_00_03->Fill(EVisLead/GeV,
                                            (xDiTau->subjetE(0) - EVisLead)/EVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 0.3 && fabs(tlvSubjetLead.Eta()) < 0.8)
        {
          h2_relE_E_subjet_mp_03_08->Fill(EVisLead/GeV,
                                            (xDiTau->subjetE(0) - EVisLead)/EVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 0.8 && fabs(tlvSubjetLead.Eta()) < 1.3)
        {
          h2_relE_E_subjet_mp_08_13->Fill(EVisLead/GeV,
                                            (xDiTau->subjetE(0) - EVisLead)/EVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 1.3 && fabs(tlvSubjetLead.Eta()) < 1.6)
        {
          h2_relE_E_subjet_mp_13_16->Fill(EVisLead/GeV,
                                            (xDiTau->subjetE(0) - EVisLead)/EVisLead);
        }
        if (fabs(tlvSubjetLead.Eta()) > 1.6 && fabs(tlvSubjetLead.Eta()) < 2.4)
        {
          h2_relE_E_subjet_mp_16_24->Fill(EVisLead/GeV,
                                            (xDiTau->subjetE(0) - EVisLead)/EVisLead);
        }
      }
      // if (nTrackSubl > 1 && nTrackTruthSubl > 1)
      if (nTrackSubl > 1)
      {
        if (fabs(tlvSubjetSubl.Eta()) < 0.3)
        {
          h2_relE_E_subjet_mp_00_03->Fill(EVisSubl/GeV,
                                            (xDiTau->subjetE(1) - EVisSubl)/EVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 0.3 && fabs(tlvSubjetSubl.Eta()) < 0.8)
        {
          h2_relE_E_subjet_mp_03_08->Fill(EVisSubl/GeV,
                                            (xDiTau->subjetE(1) - EVisSubl)/EVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 0.8 && fabs(tlvSubjetSubl.Eta()) < 1.3)
        {
          h2_relE_E_subjet_mp_08_13->Fill(EVisSubl/GeV,
                                            (xDiTau->subjetE(1) - EVisSubl)/EVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 1.3 && fabs(tlvSubjetSubl.Eta()) < 1.6)
        {
          h2_relE_E_subjet_mp_13_16->Fill(EVisSubl/GeV,
                                            (xDiTau->subjetE(1) - EVisSubl)/EVisSubl);
        }
        if (fabs(tlvSubjetSubl.Eta()) > 1.6 && fabs(tlvSubjetLead.Eta()) < 2.4)
        {
          h2_relE_E_subjet_mp_16_24->Fill(EVisSubl/GeV,
                                            (xDiTau->subjetE(1) - EVisSubl)/EVisSubl);
        }
      }


    } // if is truth matched
  } // loop over ditaus

  return EL::StatusCode::SUCCESS;
}



// ##########################################################################
int IDVariables::getNtracks (const xAOD::DiTauJet* xDiTau, int iSubjet)
{
  if (iSubjet >= xDiTau->nSubjets()) 
  {
    Warning("getNtracks()", 
      "Try to get subjet number %i but this DiTau has only %i subjets. Return 0.", 
      iSubjet, xDiTau->nSubjets());
    return 0;
  }

  if (!xDiTau->isAvailable< TrackParticleLinks_t >("trackLinks") )
  {
    Warning("getNtracks()", "Track links not available. Return 0.");
    return 0;
  } 

  int nTracks = 0;
  TrackParticleLinks_t xTracks = xDiTau->trackLinks();

  TLorentzVector tlvSubjet = TLorentzVector();
  tlvSubjet.SetPtEtaPhiE(xDiTau->subjetPt(iSubjet),
                         xDiTau->subjetEta(iSubjet),
                         xDiTau->subjetPhi(iSubjet),
                         xDiTau->subjetE(iSubjet));
  TLorentzVector tlvTrack = TLorentzVector();
  for (auto xTrack: xTracks)
  {
    tlvTrack.SetPtEtaPhiE( (*xTrack)->pt(),
                           (*xTrack)->eta(),
                           (*xTrack)->phi(),
                           (*xTrack)->e());
    if (tlvSubjet.DeltaR(tlvTrack) < xDiTau->auxdata<float>("R_subjet"))
    {
      nTracks++;
    }
  }

  return nTracks;
}


