#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoop/ProofDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>

#include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoop/OutputStream.h>

#include "DiTauRecValidation/IDVariables.h"
#include "DiTauRecValidation/DiTauEnergyScale.h"
#include "DiTauRecValidation/TriggerAnalysis.h"
#include "DiTauID/DTNTmaker.h"
#include "DiTauID/WeightsCalculator.h"

#include "DiTauRecValidation/ParallelDriver.h"

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#define CHECK( ARG )                    \
  do {                                                  \
    const bool result = ARG;                \
    if( ! result ) {                    \
      ::Error( "calcDiTauVariables", "Failed to execute: \"%s\"",  \
           #ARG );                  \
      return 1;                     \
    }                           \
  } while( false )



int main( int argc, char* argv[] ) {

  // ----------------------------------------------------------------------------
  // parse command line options
  // ----------------------------------------------------------------------------
  std::string submitDir = "submitDir";
  int nEvents = -1;
  po::options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("submitDir", po::value<std::string>(&submitDir), 
       "set submitting directory.")
    ("nEvents", po::value<int>(&nEvents)->default_value(-1),
       "set number of events to be processed")
    ("writeAOD", "use if AOD should be written out")
    ("noIDVar", "do no ID variable calculation")
    ("noDTES", "do no ditau energy scale")
    ("noDTNT", "do not write ditau n-tuple")
    ("noTrigger", "do no trigger analysis")
    ("parallel,p", "use parallel driver")  
  ;
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  // ----------------------------------------------------------------------------
  // AOD and sample handler
  // ----------------------------------------------------------------------------
  xAOD::Init().ignore();

  SH::SampleHandler sh;

  // ----------------------------------------------------------------------------
  // Signal Samples
  // ----------------------------------------------------------------------------
  SH::ScanDir()
    .samplePattern("*RS_G_hh_bbtt_hh_c10_M1800.recon.AOD.*")
    .samplePostfix("_EXT0*")
    .filePattern("user.dkirchme.*.pool.root")
    .scan(sh, "/ZIH.fast/users/kirchmeier/xAOD");

  SH::ScanDir()
    .samplePattern("*RS_G_hh_bbtt_hh_c10_M2000.recon.AOD.*")
    .samplePostfix("_EXT0*")
    .filePattern("user.dkirchme.*.pool.root")
    .scan(sh, "/ZIH.fast/users/kirchmeier/xAOD");

  SH::ScanDir()
    .samplePattern("*RS_G_hh_bbtt_hh_c10_M2250.recon.AOD.*")
    .samplePostfix("_EXT0*")
    .filePattern("user.dkirchme.*.pool.root")
    .scan(sh, "/ZIH.fast/users/kirchmeier/xAOD");


  // ----------------------------------------------------------------------------
  // Background Samples
  // ----------------------------------------------------------------------------
  // SH::ScanDir()
  //   .samplePattern("*jetjet_JZ3W.merge.AOD.*")
  //   .filePattern("AOD.*.pool.root.1")
  //   .scan(sh, "/ZIH.fast/users/kirchmeier/xAOD");
    
  // SH::ScanDir()
    // .samplePattern("*jetjet_JZ4W.recon.AOD.*")
    // .samplePostfix("_EXT0*")
    // .filePattern("user.dkirchme.*.pool.root")
    // .scan(sh, "/ZIH.fast/users/kirchmeier/xAOD");

  // SH::ScanDir()
  //   .samplePattern("*jetjet_JZ5W.recon.AOD.*")
  //   .samplePostfix("_EXT0*")
  //   .filePattern("user.dkirchme.*.pool.root")
  //   .scan(sh, "/ZIH.fast/users/kirchmeier/xAOD");

  // SH::ScanDir()
  //   .samplePattern("*jetjet_JZ6W.recon.AOD.*")
  //   .samplePostfix("_EXT0*")
  //   .filePattern("user.dkirchme.*.pool.root")
  //   .scan(sh, "/ZIH.fast/users/kirchmeier/xAOD");


  // SH::ScanDir()
  //   .samplePattern("*jetjet_JZ7W.recon.AOD.*")
  //   .samplePostfix("_EXT0*")
  //   .filePattern("user.dkirchme.*.pool.root")
  //   .scan(sh, "/ZIH.fast/users/kirchmeier/xAOD");

  // ----------------------------------------------------------------------------
  // Data Samples
  // ----------------------------------------------------------------------------
  // SH::ScanDir()
  //   .samplePattern("*data15_13TeV*")
  //   .samplePostfix("_tid*")
  //   .filePattern("AOD.*.pool.root*")
  //   .scan(sh, "/ZIH.fast/users/kirchmeier/xAOD/data");

  // ----------------------------------------------------------------------------
  // Job configurations
  // ----------------------------------------------------------------------------
  sh.setMetaString ("nc_tree", "CollectionTree");
  sh.setMetaDouble (EL::Job::optFilesPerWorker, 5);

  EL::Job job;
  job.sampleHandler (sh);
  job.options()->setDouble (EL::Job::optMaxEvents, nEvents);

  if (vm.count("writeAOD"))
  {
    EL::OutputStream outAOD("AOD", "xAOD");
    job.outputAdd(outAOD);
  }

  if (!vm.count("noIDVar"))
  {
    IDVariables *idvar = new IDVariables;
    if (vm.count("writeAOD")) idvar->writeToAOD(true);
    job.algsAdd (idvar);
  }

  if (!vm.count("noDTES"))  
  {
    DiTauEnergyScale *dtes = new DiTauEnergyScale;
    job.algsAdd (dtes);
  }

  if (!vm.count("noDTNT"))
  {
    DTNTmaker *dtnt = new DTNTmaker;
    EL::OutputStream output  ("DTNT");
    job.outputAdd (output);
    EL::NTupleSvc *ntuple = new EL::NTupleSvc ("DTNT");
    job.algsAdd (ntuple);
    job.algsAdd (dtnt);
    dtnt->outputName = "DTNT";
  }

  if (!vm.count("noTrigger"))
  {
    TriggerAnalysis* trigger = new TriggerAnalysis;
    job.algsAdd(trigger);
  }
  

  // ----------------------------------------------------------------------------
  // driver setup
  // ----------------------------------------------------------------------------
  // direct driver
  EL::DirectDriver directDriver;

  // parallel driver
  EL::ParallelDriver parallelDriver;
  parallelDriver.setupScript = 
      "/raid1/users/kirchmeier/DiTauRecValidation/setup_workernode.sh";
  parallelDriver.nCores = 7;
  parallelDriver.max_load = 50;
  parallelDriver.nice = 20;
  parallelDriver.clusterConfigFile = "/raid1/users/kirchmeier/scripts/cluster.txt";

  if (vm.count("parallel"))
  {
    parallelDriver.submit(job, submitDir);
  }
  else
  {
    directDriver.submit(job, submitDir);
  }

  std::cout << "job finished." << std::endl;

  return 0;
}
