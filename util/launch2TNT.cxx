#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoopGrid/PrunDriver.h"
#include "EventLoop/ProofDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>

#include <EventLoopAlgs/NTupleSvc.h>
#include <EventLoop/OutputStream.h>

#include "DiTauRecValidation/IDVariables.h"
#include "DiTauRecValidation/DiTauEnergyScale.h"
#include "DiTauID/DTNTmaker.h"
#include "DiTauID/WeightsCalculator.h"

#include "DiTauRecValidation/ParallelDriver.h"

#include <boost/program_options.hpp>
namespace po = boost::program_options;


#define CHECK( ARG )                    \
  do {                                                  \
    const bool result = ARG;                \
    if( ! result ) {                    \
      ::Error( "calcDiTauVariables", "Failed to execute: \"%s\"",  \
           #ARG );                  \
      return 1;                     \
    }                           \
  } while( false )



int main( int argc, char* argv[] ) {

  // ----------------------------------------------------------------------------
  // parse command line options
  // ----------------------------------------------------------------------------
  std::string submitDir = "submitDir";
  int nEvents = 100;
  po::options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("submitDir", po::value<std::string>(&submitDir), 
       "set submitting directory.")
    ("nEvents", po::value<int>(&nEvents)->default_value(100),
       "set number of events to be processed")
    ("writeAOD", "use if AOD should be written out")
    ("noIDVar", "do no ID variable calculation")
    ("noDTES", "do no ditau energy scale")
    ("noDTNT", "do not write ditau n-tuple")
    ("noTrigger", "do no trigger analysis")
  ;
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);


  // ----------------------------------------------------------------------------
  // AOD and sample handler
  // ----------------------------------------------------------------------------
  xAOD::Init().ignore();

  SH::SampleHandler sh;

  // ----------------------------------------------------------------------------
  // input files
  // ----------------------------------------------------------------------------
  // SH::addGrid(sh, "data15_13TeV.00276262.physics_Main.merge.AOD.r7562_p2521");

  // SH::addGrid(sh, "data15_13TeV.00276329.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00276336.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00276416.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00276511.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00276689.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00276731.physics_Main.merge.AOD.r7600_p2521");
  // SH::addGrid(sh, "data15_13TeV.00276778.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00276790.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00276952.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00276954.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00278880.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00278912.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00278968.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00279169.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00279259.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00279279.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00279284.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00279345.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00279515.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00279598.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00279685.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00279813.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00279867.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00279928.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00279932.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00279984.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00280231.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00280273.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00280319.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00280368.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00280423.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00280464.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00280500.physics_Main.merge.AOD.r7562_p2521");

  // SH::addGrid(sh, "data15_13TeV.00280520.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00280614.physics_Main.merge.AOD.r7562_p2521");
  
  // SH::addGrid(sh, "data15_13TeV.00280673.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00280753.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00280853.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00280862.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00280950.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00280977.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00281070.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00281074.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00281075.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00281317.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00281385.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00281411.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00282625.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00282631.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00282712.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00282784.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00282992.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00283074.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00283155.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00283270.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00283429.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00283608.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00283780.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00284006.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00284154.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00284213.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00284285.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00284420.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00284427.physics_Main.merge.AOD.r7562_p2521");
  // SH::addGrid(sh, "data15_13TeV.00284484.physics_Main.merge.AOD.r7562_p2521");

  // ttbar
  SH::addGrid(sh, "mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.AOD.e3698_a766_a822_r7676");

  // ----------------------------------------------------------------------------  
  // job configurations  
  // ----------------------------------------------------------------------------  
  sh.setMetaString ("nc_tree", "CollectionTree");
  // sh.setMetaDouble (EL::Job::optFilesPerWorker, 5);

  EL::Job job;
  job.sampleHandler (sh);
  job.options()->setDouble (EL::Job::optMaxEvents, nEvents); 

  if (vm.count("writeAOD"))
  {
    EL::OutputStream outAOD("AOD", "xAOD");
    job.outputAdd(outAOD);
  }

  if (!vm.count("noIDVar"))
  {
    std::cout << "init IDVariables" << std::endl;
    IDVariables *idvar = new IDVariables;
    if (vm.count("writeAOD")) idvar->writeToAOD(false);
    job.algsAdd (idvar);
  }
  if (!vm.count("noDTES"))
  {
    std::cout << "init DTES" << std::endl;
    DiTauEnergyScale *dtes = new DiTauEnergyScale;
    job.algsAdd (dtes);
  }
  if (!vm.count("noDTNT"))
  {
    std::cout << "init DTNTMaker" << std::endl;
    DTNTmaker *dtnt = new DTNTmaker;
    EL::OutputStream output  ("DTNT");
    job.outputAdd (output);
    EL::NTupleSvc *ntuple = new EL::NTupleSvc ("DTNT");
    job.algsAdd (ntuple);
    job.algsAdd (dtnt);
    dtnt->outputName = "DTNT";
  }

  // ----------------------------------------------------------------------------
  // driver setup
  // ----------------------------------------------------------------------------
  EL::PrunDriver driver;
  driver.options()->setString(EL::Job::optGridExcludedSite, "ANALY_LRZ");

  // comment in if you want to have an AOD output stream
  driver.options()->setString("nc_outputSampleName", "user.dkirchme.%in:name[1]%.%in:name[2]%.%in:name[6]%_test_v14");

  // driver.options()->setString("nc_outputSampleName", "user.dkirchme.%in:name[1]%.%in:name[2]%.%in:name[6]%.test.v9");

  // process the job using the driver
  std::cout<<"submit" << std::endl;
  driver.submitOnly(job, submitDir);

  return 0;
}
