#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "SampleHandler/DiskListLocal.h"
#include <TSystem.h>


#include "DiTauRecValidation/IDVariables.h"

int main( int argc, char* argv[] ) {

  // Take the submit directory from the input if provided:
  std::string submitDir = "submitDir";
  if( argc > 1 ) submitDir = argv[ 1 ];
  
  // Decide how many events to run over:
  int nEvents = 100;
  if( argc > 2 ) nEvents = atoll( argv[2] );


  // Set up the job for xAOD access:
  xAOD::Init().ignore();

  // create a new sample handler to describe the data files we use
  SH::SampleHandler sh;

  // use SampleHandler to scan all of the subdirectories of a directory for particular MC single file:
  // const char* inputFilePath = gSystem->ExpandPathName ("/ZIH.fast/users/kirchmeier/xAOD/user.dkirchme.mc15_13TeV.303367.RS_G_hh_bbtt_hh_c10_M2000.recon.ESD.e4438_s2608_r6869_v01_EXT0.56786618/");
  // SH::ScanDir().filePattern("user.dkirchme.*.pool.root").scan(sh, inputFilePath);

  const char* inputFilePath = gSystem->ExpandPathName ("/ZIH.fast/users/kirchmeier/xAOD/user.dkirchme.mc15_13TeV.361024.jetjet_JZ4W.recon.ESD.e3668_s2576_s2132_r6869_v01_EXT0.56818451/");
  SH::ScanDir().filePattern("user.dkirchme.*.pool.root").scan(sh, inputFilePath);
  
  // set the name of the tree in our files
  // in the xAOD the TTree containing the EDM containers is "CollectionTree"
  sh.setMetaString ("nc_tree", "CollectionTree");

  // further sample handler configuration may go here

  // print out the samples we found
  sh.print ();

  // this is the basic description of our job
  EL::Job job;
  job.sampleHandler (sh); // use SampleHandler in this job
  job.options()->setDouble (EL::Job::optMaxEvents, nEvents); // for testing purposes, limit to run over the first 500 events only!

  // add our algorithm to the job
  IDVariables *alg = new IDVariables;

  // later on we'll add some configuration options for our algorithm that go here

  job.algsAdd (alg);

  // make the driver we want to use:
  // this one works by running the algorithm directly:
  EL::DirectDriver driver;
  // we can use other drivers to run things on the Grid, with PROOF, etc.

  // process the job using the driver
  driver.submit (job, submitDir);
}
