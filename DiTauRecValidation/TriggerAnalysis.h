#ifndef DiTauRecValidation_TriggerAnalysis_H
#define DiTauRecValidation_TriggerAnalysis_H

#include <EventLoop/Algorithm.h>

// ROOT includes
#include <TH1.h>

// EDM includes
#include "xAODTau/DiTauJetContainer.h"

// Tools
#include "DiTauID/EventSelector.h"
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/MatchingTool.h"



class TriggerAnalysis : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  int GeV = 1000; //!

  // this is a standard constructor
  TriggerAnalysis ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();
private:
  EventSelector* m_evtSelector; //!

  Trig::TrigDecisionTool *m_trigDecisionTool; //!
  TrigConf::xAODConfigTool *m_trigConfigTool; //!
  Trig::MatchingTool* m_tmt; //!


public:
  // this is needed to distribute the algorithm to the workers
  ClassDef(TriggerAnalysis, 1);
};

#endif
