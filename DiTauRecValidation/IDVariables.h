#ifndef DiTauRecValidation_IDVariables_H
#define DiTauRecValidation_IDVariables_H

#include <EventLoop/Algorithm.h>

// ROOT includes
#include <TH1.h>

// EDM includes
#include "xAODTau/DiTauJetContainer.h"

// Tools
#include "TauAnalysisTools/TauTruthMatchingTool.h"
#include "DiTauID/DiTauIDVarCalculator.h"
#include "DiTauID/DiTauDiscriminant.h"
// #include "GoodRunsLists/GoodRunsListSelectionTool.h"
#include "DiTauID/EventSelector.h"
#include "tauRecTools/TauCalibrateLC.h"

#include <DiTauRecValidation/DiTauEnergyScale.h>

class IDVariables : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  DiTauID::DiTauIDVarCalculator* m_IDVarCalculator; //!
  TauAnalysisTools::TauTruthMatchingTool* m_T2MT; //!
  DiTauID::DiTauDiscriminant* m_DiscrTool; //!
  TauCalibrateLC* m_tauCalibTool; //!

  DiTauEnergyScale* m_dtesTool; //!

private: 

  // steering
  EventSelector* m_evtSelector; //!
  bool m_writeToAOD;
  bool isMC; //!

public:
  int m_eventCounter; //!
  int GeV = 1000; //!



  // this is a standard constructor
  IDVariables ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();
  EL::StatusCode writeToAOD(bool bWriteToAOD);
  // EL::StatusCode decorNtracks (const xAOD::DiTauJet& xDiTau);
  EL::StatusCode plotDTES (xAOD::TEvent* event,
                const xAOD::DiTauJetContainer* xDiTauContainer,
                const xAOD::TauJetContainer* xTauContainer);
  int getNtracks (const xAOD::DiTauJet* xDiTau, int iSubjet);


private:
  // histograms for reco efficiency 
  TH1 *h_ditau_true_pt; //!
  TH1 *h_ditau_match_true_pt; //!
  TH1 *h_ditau_true_pt_vis; //!
  TH1 *h_ditau_match_true_pt_vis; //!
  TH1 *h_ditau_true_dR; //!
  TH1 *h_ditau_match_true_dR; //!
  TH1 *h_ditau_true_mu; //!
  TH1 *h_ditau_match_true_mu; //!

  // histograms for energy differences for DiTauRec  
  TH1 *h_delPtRel_ditau; //!
  TH1 *h_delPtRel_subjet; //!
  TH1 *h_delPtRel_lead_subjet; //!
  TH1 *h_delPtRel_subl_subjet; //!
  
  TH2 *h2_fracE_recoE_subjet; //!
  TH2 *h2_fracE_recoE_subjet_1p; //!
  TH2 *h2_fracE_recoE_subjet_mp; //!
  TH2 *h2_fracE_recoE_subjet_2p; //!
  TH2 *h2_fracE_recoE_subjet_3p; //!
  TH2 *h2_fracE_recoE_subjet_4p; //!
  TH2 *h2_fracE_recoE_subjet_5p; //!
  TH2 *h2_fracE_recoE_subjet_1p_00_03; //!
  TH2 *h2_fracE_recoE_subjet_1p_03_08; //!
  TH2 *h2_fracE_recoE_subjet_1p_08_13; //!
  TH2 *h2_fracE_recoE_subjet_1p_13_16; //!
  TH2 *h2_fracE_recoE_subjet_1p_16_24; //!
  TH2 *h2_fracE_recoE_subjet_mp_00_03; //!
  TH2 *h2_fracE_recoE_subjet_mp_03_08; //!
  TH2 *h2_fracE_recoE_subjet_mp_08_13; //!
  TH2 *h2_fracE_recoE_subjet_mp_13_16; //!
  TH2 *h2_fracE_recoE_subjet_mp_16_24; //!
  
  TH2 *h2_relE_E_subjet_1p_00_03; //!
  TH2 *h2_relE_E_subjet_1p_03_08; //!
  TH2 *h2_relE_E_subjet_1p_08_13; //!
  TH2 *h2_relE_E_subjet_1p_13_16; //!
  TH2 *h2_relE_E_subjet_1p_16_24; //!
  TH2 *h2_relE_E_subjet_mp_00_03; //!
  TH2 *h2_relE_E_subjet_mp_03_08; //!
  TH2 *h2_relE_E_subjet_mp_08_13; //!
  TH2 *h2_relE_E_subjet_mp_13_16; //!
  TH2 *h2_relE_E_subjet_mp_16_24; //!

  // this is needed to distribute the algorithm to the workers
  ClassDef(IDVariables, 1);
};

#endif
