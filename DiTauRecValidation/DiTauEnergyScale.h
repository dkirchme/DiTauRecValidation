#ifndef DiTauRecValidation_DiTauEnergyScale_H
#define DiTauRecValidation_DiTauEnergyScale_H

#include <EventLoop/Algorithm.h>

// ROOT includes
#include <TH1.h>
#include <TH2.h>

// EDM includes
#include "xAODTau/DiTauJetContainer.h"
#include "xAODTau/TauJetContainer.h"
#include "xAODTau/DiTauJet.h"

#include "DiTauID/EventSelector.h"


class DiTauEnergyScale : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;



  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
  EL::StatusCode plotEnergyDiff(xAOD::TEvent* event, 
                                const xAOD::DiTauJetContainer* xDiTauContainer,
                                const xAOD::TauJetContainer* xTauContainer);

private:
  int getNtracks (const xAOD::DiTauJet* xDiTau, int iSubjet);

  // steering 
  EventSelector* m_evtSelector; //!


public:
  // histograms for reco efficiency 
  TH1 *h_ditau_true_pt; //!
  TH1 *h_ditau_match_true_pt; //!
  TH1 *h_ditau_true_pt_vis; //!
  TH1 *h_ditau_match_true_pt_vis; //!
  TH1 *h_ditau_true_dR; //!
  TH1 *h_ditau_match_true_dR; //!
  TH1 *h_ditau_true_mu; //!
  TH1 *h_ditau_match_true_mu; //!

  // histograms for energy differences for DiTauRec
  TH1 *h_delPt_ditau; //!
  TH1 *h_delPt_subjet; //!
  TH1 *h_delPt_lead_subjet; //!
  TH1 *h_delPt_subl_subjet; //!
  
  TH1 *h_delPtRel_ditau; //!
  TH1 *h_delPtRel_subjet; //!
  TH1 *h_delPtRel_lead_subjet; //!
  TH1 *h_delPtRel_subl_subjet; //!
  
  TH2 *h2_delPt_pt_ditau; //!
  TH2 *h2_delPt_pt_subjet; //!
  TH2 *h2_delPt_pt_lead_subjet; //!
  TH2 *h2_delPt_pt_subl_subjet; //!

  TH2 *h2_delPt_mu_ditau; //!
  TH2 *h2_delPt_mu_subjet; //!
  TH2 *h2_delPt_mu_lead_subjet; //!
  TH2 *h2_delPt_mu_subl_subjet; //!
  TH2 *h2_delPt_eta_subjet; //!

  TH2 *h2_fracPt_recoPt_subjet; //!
  TH2 *h2_fracPt_recoPt_subjet_1p; //!
  TH2 *h2_fracPt_recoPt_subjet_mp; //!
  TH2 *h2_fracPt_recoPt_subjet_1p_00_03; //!
  TH2 *h2_fracPt_recoPt_subjet_1p_03_08; //!
  TH2 *h2_fracPt_recoPt_subjet_1p_08_13; //!
  TH2 *h2_fracPt_recoPt_subjet_1p_13_16; //!
  TH2 *h2_fracPt_recoPt_subjet_1p_16_24; //!
  TH2 *h2_fracPt_recoPt_subjet_mp_00_03; //!
  TH2 *h2_fracPt_recoPt_subjet_mp_03_08; //!
  TH2 *h2_fracPt_recoPt_subjet_mp_08_13; //!
  TH2 *h2_fracPt_recoPt_subjet_mp_13_16; //!
  TH2 *h2_fracPt_recoPt_subjet_mp_16_24; //!

  TH2 *h2_relPt_pt_subjet_1p_00_03; //!
  TH2 *h2_relPt_pt_subjet_1p_03_08; //!
  TH2 *h2_relPt_pt_subjet_1p_08_13; //!
  TH2 *h2_relPt_pt_subjet_1p_13_16; //!
  TH2 *h2_relPt_pt_subjet_1p_16_24; //!
  TH2 *h2_relPt_pt_subjet_mp_00_03; //!
  TH2 *h2_relPt_pt_subjet_mp_03_08; //!
  TH2 *h2_relPt_pt_subjet_mp_08_13; //!
  TH2 *h2_relPt_pt_subjet_mp_13_16; //!
  TH2 *h2_relPt_pt_subjet_mp_16_24; //!

  // histograms for energy differences for tauRec
  TH1 *h_delPt_tau; //!
  TH1 *h_delPtRel_tau; //!
  
  TH2 *h2_delPt_pt_tau; //!
  TH2 *h2_delPt_mu_tau; //!
  TH2 *h2_delPtRel_eta_tau; //!


  int m_eventCounter; //!
  int GeV = 1000; //!


  // this is a standard constructor
  DiTauEnergyScale ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(DiTauEnergyScale, 1);
};

#endif
