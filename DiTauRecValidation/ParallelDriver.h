#pragma once

#include <EventLoop/Global.h>
#include <EventLoop/BatchDriver.h>
#include <SampleHandler/Global.h>

namespace EL
{
  class ParallelDriver : public BatchDriver
  {
  public:
    ParallelDriver ();
    void testInvariant () const;

    //configurables
  public:
    size_t nCores = 3;
    std::string clusterConfigFile = "";
    std::string setupScript = "";
    int nice = 10;
    size_t max_load = 60; //percent
    size_t nRetries = 0;

  private:
    virtual void
    batchSubmit (const std::string& location, const SH::MetaObject& options,
                 std::size_t njob) const;

    ClassDef(ParallelDriver, 1);
  };
}