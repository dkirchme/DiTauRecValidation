#ifndef DiTauRecValidation_RecoEfficiency_H
#define DiTauRecValidation_RecoEfficiency_H

#include <EventLoop/Algorithm.h>
#include <TH1.h>
#include "TLorentzVector.h"
#include "xAODTau/DiTauJetContainer.h"

class RecoEfficiency : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;



  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  TH1 *h_ditau_pt; //!
  TH1 *h_ditau_true_pt; //!
  TH1 *h_ditau_match_true_pt; //!
  TH1 *h_ditau_true_dR; //!
  TH1 *h_ditau_match_true_dR; //!
  TH1 *h_lead_subjet_pt; //!
  TH1 *h_subl_subjet_pt; //!
  TH1 *h_ditau_lead_subjet_fcore; //!
  TH1 *h_ditau_subl_subjet_fcore; //!
  TH1 *h_ditau_match_lead_subjet_fcore; //!
  TH1 *h_ditau_match_subl_subjet_fcore; //!
  int m_eventCounter; //!
  int GeV = 1000; //!



  // this is a standard constructor
  RecoEfficiency ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();
  virtual EL::StatusCode getTruthTaus (std::vector<TLorentzVector*>&);
  bool isTruthMatched (const xAOD::DiTauJet*, std::vector<TLorentzVector*>);

  // this is needed to distribute the algorithm to the workers
  ClassDef(RecoEfficiency, 1);
};

#endif
